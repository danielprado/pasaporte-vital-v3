<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActividadInteres extends Model
{
    //
    protected $table = 'tbl_actividades_interes';
	protected $primaryKey = 'i_pk_id';
	protected $fillable = ['vc_nombre', 'i_estado'];
	protected $connection = '';
}
