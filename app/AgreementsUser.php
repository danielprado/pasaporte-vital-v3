<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgreementsUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_agreements_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['beneficiary_id', 'user_id', 'agreement_id'];
}
