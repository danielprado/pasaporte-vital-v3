<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgreementsView extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_agreements_view';
}
