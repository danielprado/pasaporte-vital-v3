<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyAgreementView extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_company_agreement_view';
}
