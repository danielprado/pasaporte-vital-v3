<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class DatosPersonaAnterior extends Eloquent {

    protected $table = 'pasaporte_registro';
    protected $primaryKey = 'documento';
    protected $fillable = ['tipoDocumento','apellidos','nombres','tipoDocumento'];
    protected $connection = 'pasaporte_anterior';
    public $timestamps = false;

    public function getFullNameAttribute()
    {
        $n1 = isset( $this->nombres ) ? trim( $this->nombres ) : '';
        $n2 = isset( $this->apellidos ) ? trim( $this->apellidos ) : '';

        return $n1.' '.$n2;
    }

}