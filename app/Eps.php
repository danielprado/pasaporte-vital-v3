<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eps extends Model
{
    protected $table = 'tbl_eps';
    protected $primaryKey = 'i_pk_id';
    protected $fillable = ['vc_nombre', 'i_estado'];
}
