<?php

namespace App\Exceptions;

use App\Traits\ApiResponse;
use ErrorException;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Connection;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use MongoDB\Driver\Exception\ServerException;
use PDOException;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\TooManyRequestsHttpException;

class Handler extends ExceptionHandler
{

    use ApiResponse;

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthenticationException::class,
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        TokenMismatchException::class,
        ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];


    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Exception $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Exception $exception
     * @return Response
     */
    public function render($request, Exception $exception)
    {
        if ( env('APP_DEBUG') == true ) {
            return parent::render($request, $exception);
        }
        /*
         * Api Request
         */

        if ( $request->wantsJson() ) {

            if ($exception instanceof ValidationException)
                return $this->convertValidationExceptionToResponse( $exception, $request );

            if ($exception instanceof ModelNotFoundException)
                return $this->errorResponse(trans('validation.handler.resource_not_found'), 404);

            if ($exception instanceof AuthenticationException)
                return $this->unauthenticated($request, $exception);

            if ($exception instanceof NotFoundHttpException)
                return $this->errorResponse(trans('validation.handler.resource_not_found_url'), 404);

            if ($exception instanceof AuthorizationException)
                return $this->errorResponse(trans('validation.handler.unauthorized'), 403);

            if ($exception instanceof MethodNotAllowedHttpException)
                return $this->errorResponse(trans('validation.handler.method_allow'), 405);


            if ( $exception instanceof Connection) {
                return $this->errorResponse(trans('validation.handler.unexpected_failure'), 500);
            }

            if ($exception instanceof HttpException)
                return $this->errorResponse($exception->getMessage(), $exception->getStatusCode());

            if ( $exception instanceof ErrorException )
                return $this->errorResponse(trans('validation.handler.unexpected_failure'), 500);

            if ($exception instanceof PDOException)
                return $exception->getCode();

            if ($exception instanceof FatalThrowableError)
                return $this->errorResponse(trans('validation.handler.conflict'), 409);

            if ($exception instanceof QueryException) {

                if ($exception->errorInfo[0] === "23503")
                    return $this->errorResponse(trans('validation.handler.relation_not_delete'), 409);

                if ($exception->errorInfo[0] === "42S22")
                    return $this->errorResponse(trans('validation.handler.column_not_found'), 409);

                if ($exception->errorInfo[0] === "42S02")
                    return $this->errorResponse(trans('validation.handler.column_not_found'), 409);

                if ($exception->errorInfo[1] == 2002)
                    return $this->errorResponse( trans('validation.handler.connection_refused', ['db' => 'MySQL'] ), 405);

                if ($exception->errorInfo[1] == 1451)
                    return $this->errorResponse(trans('validation.handler.relation_not_delete'), 409);

                if ($exception->errorInfo[1] == 7)
                    return $this->errorResponse(trans('validation.handler.conflict'), 409);

                if ($exception->getCode() == 7)
                    return $this->errorResponse(trans('validation.handler.service_unavailable'), 503);
            }

            if ($exception instanceof TokenMismatchException)
                return $this->errorResponse(trans('validation.handler.token_mismatch'), 422);

            if ($exception instanceof TooManyRequestsHttpException)
                return $this->errorResponse(trans('validation.handler.max_attempts'), 429);
        }

        /**
         * Request Web
         */
        if ($exception instanceof TokenMismatchException)
            return redirect()->back()->withInput($request->input());

        if ($exception instanceof QueryException) {
                return response()->view('errors.503');
        }

        if ($exception instanceof AuthenticationException)
            return $this->unauthenticated($request, $exception);

        if ( $exception instanceof ServerException || $exception instanceof ErrorException) {
            $status = $exception->getCode();
            if (view()->exists("errors.{$status}")){
                return response()->view("errors.{$status}");
            } else {
                return response()->view("errors.500");
            }
        }

        if ($exception instanceof PDOException) {
            if ($exception->getCode() == 7)
                return response()->view('errors.503');

            if ($exception->getCode() == 2002)
                return $this->errorResponse( trans('validation.handler.connection_refused', ['db' => 'MySQL'] ), 405);
        }

        if ($exception instanceof HttpException) {
            $status = $exception->getStatusCode();
            if (view()->exists("errors.{$status}"))
                return response()->view("errors.{$status}", ['exception' => $exception], $status, $exception->getHeaders());
        }

        return parent::render($request, $exception);
    }

    /**
     * Create a response object from the given validation exception.
     *
     * @param ValidationException $e
     * @param  Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {
        return $request->wantsJson()
            ? $this->errorResponse(
                $e->validator->errors()->getMessages(),
                422
            )
            : parent::render($request, $e);
    }

    /**
     * Convert an authentication exception into a response.
     *
     * @param  Request  $request
     * @param AuthenticationException $exception
     * @return Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return $request->wantsJson()
            ?   $this->errorResponse(trans('validation.handler.unauthenticated'), 401)
            :   redirect()->guest(route('logout'));
    }
}
