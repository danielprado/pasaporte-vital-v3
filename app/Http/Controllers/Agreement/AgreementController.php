<?php

namespace App\Http\Controllers\Agreement;

use App\Agreement;
use App\AgreementsUser;
use App\AgreementsView;
use App\Transformers\AgreementTransformer;
use App\Transformers\AgreementViewTransformer;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class AgreementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $company
     * @return \Illuminate\Http\Response
     */
    public function index( $company )
    {
        $resource = new Collection(Agreement::query()->where('company_id', $company)->get(), new AgreementTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( Requests\StoreAgreementCompanyRequest $request )
    {
        $agreement = new Agreement();
        $agreement->company_id = $request->get('company_id');
        $agreement->agreement = $request->get('agreement');
        if ( $agreement->save() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success_save'),
                'code'  => 200
            ], 200);
        }

        return response()->json([
            'message' =>  trans('validation.handler.unexpected_failure'),
            'code'  =>  422
        ], 422);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Requests\StoreAgreementCompanyRequest $request
     * @param Agreement $agreement
     * @return \Illuminate\Http\JsonResponse
     */
    public function update( Requests\StoreAgreementCompanyRequest $request, Agreement $agreement )
    {
        $agreement->company_id = $request->get('company_id');
        $agreement->agreement = $request->get('agreement');
        if ( $agreement->save() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success_save'),
                'code'  => 200
            ], 200);
        }

        return response()->json([
            'message' =>  trans('validation.handler.unexpected_failure'),
            'code'  =>  422
        ], 422);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreAgreementRequest $request)
    {
        $agreement = new AgreementsUser();
        $agreement->beneficiary_id = $request->get('beneficiary_id');
        $agreement->user_id = $_SESSION['Id_funcionario'];
        $agreement->agreement_id = $request->get('agreement');

        if ( $agreement->save() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success', ['passport' => $request->get('passport')]),
                'code'  => 200
            ], 200);
        }

        return response()->json([
            'message' =>  trans('validation.handler.unexpected_failure'),
            'code'  =>  422
        ], 422);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function table()
    {
        $column = \request()->has( 'column' ) ? \request()->get('column') : 'id';
        $order = \request()->has( 'order' ) ? \request()->get('order') : false;
        $per_page = \request()->has( 'per_page' ) ? \request()->get('per_page') : 5;
        $query = \request()->has( 'query' ) ? \request()->get('query') : null;

        $order =  ( $order ) ? 'asc' : 'desc';

        $data = AgreementsView::query();

        if ( $query ) {
            $data = $data->where('id', 'LIKE', "%{$query}%")
                ->orWhere('company', 'LIKE', "%{$query}%")
                ->orWhere('user', 'LIKE', "%{$query}%")
                ->orWhere('beneficiary', 'LIKE', "%{$query}%")
                ->orWhere('agreement', 'LIKE', "%{$query}%");
        }

        $data = $data->orderBy( $column, $order )->paginate( $per_page );

        $resource = $data->getCollection()
            ->map(function($model) {
                return ( new AgreementViewTransformer() )->transform( $model );
            })->toArray();

        $table = new LengthAwarePaginator(
            $resource,
            (int) $data->total(),
            (int) $data->perPage(),
            (int) $data->currentPage(), [
            'path' => request()->url(),
            'query' => [
                'page' => (int) $data->currentPage()
            ]
        ]);

        return response()->json([
            'data'  =>  $table,
            'code'  =>  200
        ], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Agreement $agreements
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Agreement $agreements)
    {
        $agreements->delete();
        return response()->json([
            'data'  => trans('validation.handler.agreement_deleted'),
            'code'  =>  200
        ],200);
    }

    public function destroyAgreementUser( AgreementsUser $agreement )
    {
        $agreement->delete();
        return response()->json([
            'data'  => trans('validation.handler.agreement_deleted'),
            'description' =>    'User',
            'code'  =>  200
        ],200);
    }
}
