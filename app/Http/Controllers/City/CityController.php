<?php

namespace App\Http\Controllers\City;

use App\Ciudad;
use App\Transformers\CityTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $country
     * @return void
     */
    public function index($country)
    {
        $data = Ciudad::where('Id_Pais', $country)->get();
        if ( count( $data ) == 0 ) {
            $data = Ciudad::where('Id_Ciudad', 1123)->get();
        }
        $resource = new Collection($data, new CityTransformer);
        $manager = new Manager();
        $rootScope = $manager->createData($resource);

        return response()->json($rootScope->toArray(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
