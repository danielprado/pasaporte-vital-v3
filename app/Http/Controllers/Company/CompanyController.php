<?php

namespace App\Http\Controllers\Company;

use App\Company;
use App\CompanyAgreementView;
use App\Http\Requests\StoreCompanyRequest;
use App\Transformers\CompanyAgreementViewTransformer;
use App\Transformers\CompanyTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resource = new Collection(Company::all(), new CompanyTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function table()
    {
        $column = \request()->has( 'column' ) ? \request()->get('column') : 'id';
        $order = \request()->has( 'order' ) ? \request()->get('order') : false;
        $per_page = \request()->has( 'per_page' ) ? \request()->get('per_page') : 5;
        $query = \request()->has( 'query' ) ? \request()->get('query') : null;

        $order =  ( $order ) ? 'asc' : 'desc';

        $data = CompanyAgreementView::query();

        if ( $query ) {
            $data = $data->where('id', 'LIKE', "%{$query}%")
                ->orWhere('company', 'LIKE', "%{$query}%")
                ->orWhere('agreement', 'LIKE', "%{$query}%");
        }

        $data = $data->orderBy( $column, $order )->paginate( $per_page );

        $resource = $data->getCollection()
            ->map(function($model) {
                return ( new CompanyAgreementViewTransformer() )->transform( $model );
            })->toArray();

        $table = new LengthAwarePaginator(
            $resource,
            (int) $data->total(),
            (int) $data->perPage(),
            (int) $data->currentPage(), [
            'path' => request()->url(),
            'query' => [
                'page' => (int) $data->currentPage()
            ]
        ]);

        return response()->json([
            'data'  =>  $table,
            'code'  =>  200
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCompanyRequest $request)
    {
        $company = new Company();
        $company->company = $request->get('company');

        if ( $company->save() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success_save'),
                'code'  => 200
            ], 200);
        }

        return response()->json([
            'message' =>  trans('validation.handler.unexpected_failure'),
            'code'  =>  422
        ], 422);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
