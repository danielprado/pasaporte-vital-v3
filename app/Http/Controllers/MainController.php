<?php 

namespace App\Http\Controllers;

use App\ActivityAccess;
use Idrd\Usuarios\Repo\Acceso;
use Idrd\Usuarios\Repo\ActividadesSim;
use Idrd\Usuarios\Repo\PersonaInterface;
use Illuminate\Http\Request;

class MainController extends Controller {

	protected $Usuario;
	protected $repositorio_personas;

	public function __construct(PersonaInterface $repositorio_personas)
	{
		if (isset($_SESSION['Usuario']))
			$this->Usuario = $_SESSION['Usuario'];

		$this->repositorio_personas = $repositorio_personas;
	}

    public function auth()
    {
        if ( isset( $_SESSION['Usuario'] ) ) {
            $user = $_SESSION['auth'];
            $username = Acceso::query()->where('Id_Persona', $user->Id_Persona)->first();
            return response()->json([
                'data'  =>  [
                    'username'      =>  $user->part_name,
                    'auth'          =>  isset( $username->Usuario ) ? $username->Usuario : null,
                    'permissions'   =>  $_SESSION['Usuario']['Permisos']
                ],
                'code'  =>  200
            ], 200);
        }

        return response()->json([
            'message'  =>  trans('validation.handler.unauthenticated'),
            'code'  =>  401
        ], 401);
	}

    public function check()
    {
        return response()->json([
            'data'  =>  isset( $_SESSION['auth']),
            'code'  =>  200
        ], 200);
	}
	
    public function index(Request $request)
    {
        //$fake_permissions = [1046, 1 , 1 , 1 , 1 , 1 , 1 ];
        $fake_permissions = null;

        $_SESSION['Id_funcionario'] ='';
        if ($request->has('vector_modulo')  || $fake_permissions )
        {
            $data = $request->has('vector_modulo') ? $request->get('vector_modulo') : $fake_permissions;
            $encode = $request->has('vector_modulo') ? true : false;
            $this->setData( $data, $encode );
        }

        if ( !isset($_SESSION['auth']) ) {
            return redirect()->route('welcome');
        }

        $user = $_SESSION['auth'];
        $username = Acceso::query()->where('Id_Persona', $user->Id_Persona)->first();
        $array = [
            'user'  => [
                'username'      =>  $user->part_name,
                'auth'          =>  isset( $username->Usuario ) ? $username->Usuario : null,
                'permissions'   =>  $_SESSION['Usuario']['Permisos']
            ]
        ];

        return redirect()->route('welcome');
    }

    public function welcome()
    {
        return view('welcome');
    }

    public function setData( $data, $encode = true )
    {
        $vector = $encode ? urldecode( $data ) : $data;
        $user_array = is_array($vector) ? $vector : unserialize($vector);
        $permissions_array = $user_array;

        $permisos = [
            'create_or_edit_passport' => intval($permissions_array[1]),
            'print_passport' => intval($permissions_array[2]),
            'create_general_report' => intval($permissions_array[3]),
            'create_renew_report' => intval($permissions_array[4]),
            'create_own_report' => intval($permissions_array[5]),
            'view_passports' => intval($permissions_array[6]),
            'delete_passport' => intval($permissions_array[7]),
            'create_agreement' => intval($permissions_array[8]),
            'view_agreement' => intval($permissions_array[9]),
            'agreement_report' => intval($permissions_array[10]),
        ];
        $_SESSION['Usuario'] = $user_array;

        $funcionario = $this->repositorio_personas->obtener($_SESSION['Usuario'][0]);
        $_SESSION['Id_funcionario']= $_SESSION['Usuario'][0];
        $_SESSION['Usuario']['Recreopersona'] = [];
        $_SESSION['Usuario']['Roles'] = [];
        $_SESSION['Usuario']['Persona'] = $funcionario;
        $_SESSION['Usuario']['Permisos'] = $permisos;
        $this->Usuario = $_SESSION['Usuario'];
        $_SESSION['auth'] = auth()->loginUsingId( $_SESSION['Usuario'][0] );
    }

	public function logout(Request $request)
	{
        session_destroy();
        auth()->logout();

        if ( $request->ajax()  ) {
            return response()->json([
                'data'  =>  'Redirecting',
                'code'  =>  200
            ], 200);
        }
        return  redirect()->route('welcome');
	}

    public function login(Request $request)
    {
        if ( $request->has('username') && $request->has('password') ) {
            $access = Acceso::query()->where([
                [ 'Usuario', $request->get('username') ],
                ['Contrasena', sha1( $this->cifrar( $request->get('password') ) )]
            ])->first();

            if ( $access ) {
                $module_id = (int) env('MODULE_ID'); //Módulo de Pasaporte Vital 2.0
                //$permissions  = [791, 792, 793, 794, 795, 796]; //Permisos Pasaporte Vital
                $permissions = ActividadesSim::query()->where( 'Id_Modulo', $module_id )->get()->pluck('Id_Actividad')->toArray();
                $perms = ActivityAccess::query()->whereIn('Id_Actividad', $permissions)->where('Id_Persona', $access->Id_Persona)->get()->pluck('Estado', 'Id_Actividad')->toArray();

                if ( count( $perms ) > 0 ) {
                    $vector = [
                        (int) $access->Id_Persona,
                        (int) $perms[791],    //registro_pasaporte
                        (int) $perms[792],    //Imprimir_pasaporte
                        (int) $perms[793],    //Reporte_general
                        (int) $perms[794],    //Impresiones_realizadas
                        (int) $perms[795],    //Mis_registros
                        (int) $perms[796],    //Consulta_Pasaporte
                        (int) $perms[797],    //Eliminar_Pasaporte
                        (int) $perms[810],    //Crear Compromiso
                        (int) $perms[811],    //Ver Convenios
                        (int) $perms[812],    //Reporte Convenios
                    ];
                    $this->setData(  $vector, false );
                    return ( $request->ajax() )
                            ? response()->json([
                                    'data'  =>  'Redirecting',
                                    'code'  =>  200
                                ], 200)
                            : redirect()->route('welcome');
                }
                return   ( $request->ajax() )
                        ? response()->json([
                            'message'  =>  trans('validation.handler.unauthorized'),
                            'code'  =>  403
                        ], 403)
                        : view('errors.403');
            } else {
                return   ( $request->ajax() )
                    ? response()->json([
                            'message'  =>  trans('auth.failed'),
                            'code'  =>  422
                        ], 422)
                    : redirect()->back();
            }
        }
        return redirect()->route('welcome');
	}

    function cifrar($M)
    {
        $k = 18;
        $C = '';
        for($i=0; $i<strlen($M); $i++)$C.=chr((ord($M[$i])+$k)%255);
        return $C;
    }

    function decifrar($C)
    {
        $k = 18;
        $M = '';
        for($i=0; $i<strlen($C); $i++)$M.=chr((ord($C[$i])-$k+255)%255);
        return $M;
    }
}