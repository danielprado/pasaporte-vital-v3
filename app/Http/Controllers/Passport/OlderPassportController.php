<?php


namespace App\Http\Controllers\Passport;


use App\ActividadInteres;
use App\Ciudad;
use App\DatosPersonaAnterior;
use App\Genero;
use App\Http\Controllers\Controller;
use App\Http\Requests\OlderPasaporteRequest;
use App\Pais;
use App\PasaporteAnterior;
use App\PasaporteEncuestaAnterior;
use App\Persona;
use App\Renew;
use App\SuperCade;
use Idrd\Usuarios\Repo\Localidad;
use Idrd\Usuarios\Repo\Documento;
use Idrd\Usuarios\Repo\Eps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OlderPassportController extends Controller
{

    public function index(OlderPasaporteRequest $request)
    {
        if ( $request->has('query') ) {
            $passport = PasaporteAnterior::query()->where('idPasaporte', $request->get('query'))
                                                  ->orWhere('documento', $request->get('query'))
                                                  ->first();
            if ( $passport ) {
                $document = isset( $passport->documento ) ? $passport->documento : null;
                $person = Persona::query()->where('Cedula', $document )->first();
                $register = DatosPersonaAnterior::find( $document );

                return response()->json([
                        'data'  =>  $this->toArray( $person, $register, $passport, $document ),
                        'code'  =>  200
                    ], 200);
            }

            return response()->json( [
                'message'   => trans('validation.handler.resource_not_found'),
                'code'      => 404
            ], 404 );
        }

        if ( $request->has('document') ) {
            $passport = PasaporteAnterior::query()->where('documento', $request->get('document'))->first();

            if ( $passport ) {
                $person = Persona::query()->where('Cedula',$request->get('document') )->first();
                $register = DatosPersonaAnterior::find($request->get('document') );
                return response()->json([
                    'data'  =>  $this->toArray( $person, $register, $passport, $request->get('document') ),
                    'code'  =>  200
                ], 200);
            }

            return response()->json( [
                'message'   => trans('validation.handler.resource_not_found'),
                'code'      => 404
            ], 404 );
        }

        return response()->json( [
            'message'   => trans('validation.handler.resource_not_found'),
            'code'      => 404
        ], 404 );
    }

    public function destroy( $id )
    {
        Renew::query()->where('i_fk_id_pasaporte', $id )->delete();
        $passport = PasaporteAnterior::query()->where('idPasaporte', $id)->first();
        if ( $passport ) {
            PasaporteEncuestaAnterior::query()->where( 'documento', $passport->documento )->delete();
            DB::connection('pasaporte_anterior')->table('pasaporte_registro')->where( 'documento', $passport->documento )->delete();
            $passport->delete();
            return response()->json([
                'data'  => trans('validation.handler.deleted', ['passport' => $passport->idPasaporte]),
                'code'  =>  200
            ],200);
        }
        return response()->json([
            'data'  => trans('validation.handler.resource_not_found_url'),
            'code'  =>  404
        ],404);
    }

    public function toArray( $person, $register, $passport, $document )
    {
        return [
            'id'    =>  isset( $passport->idPasaporte ) ? (int) $passport->idPasaporte : null,
            'user_cade' =>  isset( $passport->id_Operario ) ? (int) $this->getPersona( $passport->id_Operario )['id'] : null,
            'user'  =>  isset( $person->Id_Persona ) ? (int) $person->Id_Persona : null,
            'supercade' =>  isset( $passport->supercade ) ? $this->getSuperCadeId( $passport->supercade ) : null,
            'retired'   =>  isset( $register->pensionado ) ? ucfirst( strtolower( $register->pensionado ) ) : null,
            'location'  =>  isset( $register->localidad ) ? (int) $register->localidad : null,
            'stratum'   =>  isset( $register->estrato ) ? (int) $register->estrato : null,
            'address'   =>  isset( $register->direccionResidencia ) ? $this->toUpper( $register->direccionResidencia ) : null,
            'phone' =>  isset( $register->telFijo ) ? $register->telFijo : null,
            'mobile'    =>  isset( $register->telCelular ) ? $register->telCelular : null,
            'hobbies'   =>  isset( $register->actividadesInteres ) ? (int) $register->actividadesInteres : null,
            'eps'   =>  isset( $register->tipoSeguridad ) ? (int) $register->tipoSeguridad : null,
            'observation'  =>  isset( $register->observaciones ) ? $register->observaciones : null,
            'question_1'    =>  $this->getSurvey( $document )['question_1'],
            'question_2'    =>  $this->getSurvey( $document )['question_2'],
            'question_3'    =>  $this->getSurvey( $document )['question_3'],
            'question_4'    =>  $this->getSurvey( $document )['question_4'],
            'hobbies_name'    =>  isset( $register->actividadesInteres ) ? $this->getActivities($register->actividadesInteres) : null,
            'supercade_name'  =>  isset( $passport->supercade ) ? $passport->supercade : null,
            'eps_name'  =>  isset(  $register->tipoSeguridad ) ?  $this->getEps( $register->tipoSeguridad ) : null,
            'document' =>  isset( $person->Cedula ) ? (int) $person->Cedula :  isset( $register->documento ) ? $register->documento : null,
            'document_type' =>  isset( $person->Id_TipoDocumento ) ? (int) $person->Id_TipoDocumento :  isset( $register->tipoDocumento ) ? (int) $register->tipoDocumento : null,
            'full_name'  =>  isset( $person->full_name ) ? $this->toUpper( $person->full_name ) :  isset( $register->full_name ) ? $this->toUpper( $register->full_name ) : null,
            //'first_name'  =>  isset( $person->Primer_Nombre ) ? $this->toUpper( $person->Primer_Nombre ) :  isset( $register->nombres ) ? $this->toUpper( $register->nombres ) : null,
            'first_name'  =>  isset( $person->Primer_Nombre ) ? $this->toUpper( $person->Primer_Nombre ) :  $this->toUpper( $register->nombres ),
            'middle_name'  =>  isset( $person->Segundo_Nombre ) ? $this->toUpper( $person->Segundo_Nombre ) : null,
            'first_last_name'  =>  isset( $person->Primer_Apellido ) ? $this->toUpper( $person->Primer_Apellido ) :  $this->toUpper( $register->apellidos ) ,
            //'first_last_name'  =>  isset( $person->Primer_Apellido ) ? $this->toUpper( $person->Primer_Apellido ) :  isset( $register->apellidos ) ? $this->toUpper( $register->apellidos ) : null,
            'second_last_name'  =>  isset( $person->Segundo_Apellido ) ? $this->toUpper( $person->Segundo_Apellido ) : null,
            'birthday'   =>  isset( $person->Fecha_Nacimiento ) ? $person->Fecha_Nacimiento :  isset( $register->fechaNacimiento ) ? $register->fechaNacimiento : null,
            'country'    =>  isset( $person->Id_Pais ) ? (int) $person->Id_Pais :  isset( $register->paisNacimiento ) ? $register->paisNacimiento : null,
            'gender'  =>  isset( $person->Id_Genero ) ? (int) $person->Id_Genero :  isset( $register->sexo ) ? $register->sexo : null,
            'city'    =>  isset( $person->i_fk_id_ciudad ) ? (int) $person->i_fk_id_ciudad :  null,
            'document_type_name'   =>  isset( $person->Id_TipoDocumento ) ? $this->getDocumentType( $person->Id_TipoDocumento ):  isset( $register->tipoDocumento ) ? $this->getDocumentType( $register->tipoDocumento ) : null,
            'gender_name'  =>  isset( $person->Id_Genero ) ? $this->getGender( $person->Id_Genero ):  isset( $register->sexo ) ? $this->getGender( $register->sexo ) : null,
            'country_name'    =>  isset( $person->Id_Pais ) ? $this->getCountry( $person->Id_Pais ):  isset( $register->paisNacimiento ) ? $this->getCountry($register->paisNacimiento) : null,
            'city_name'    =>  isset( $person->i_fk_id_ciudad ) ? $this->getCity( $person->i_fk_id_ciudad ):  null,
            'location_name' =>  isset( $register->localidad ) ? $this->getLocation( $register->localidad ): null,
            'user_cade_name' =>  isset( $passport->id_Operario ) ?  $this->getPersona( $passport->id_Operario )['name'] : null,
            'user_cade_document'    =>  isset( $passport->id_Operario ) ? $passport->id_Operario  : null,
            'renew'    =>  isset( $passport->Renovacion ) ? $passport->Renovacion + Renew::query()->where('i_fk_id_pasaporte', isset( $passport->idPasaporte ) ? (int) $passport->idPasaporte : 0)->get()->count()  : 0,
            'created_at'    =>  isset( $passport->fechaExpedicion, $passport->horaExpedicion ) ? $passport->fechaExpedicion.' '.$passport->horaExpedicion : null,
        ];
    }

    public function getPersona( $document )
    {
        $person = Persona::query()->where('Cedula', $document)->first();
        $array = [
            'id'    =>  null,
            'name'  =>  null
        ];
        if ( $person ) {
            $array['id'] = isset( $person->Id_Persona ) ? (int) $person->Id_Persona : 0;
            $array['name'] = isset( $person->full_name ) ? $this->toUpper( $person->full_name ) : 0;
        }

        return $array;
    }

    public function getSurvey( $document )
    {
        $survey = PasaporteEncuestaAnterior::find($document);
        $array = [
            'question_1'    =>  null,
            'question_2'    =>  null,
            'question_3'    =>  null,
            'question_4'    =>  null,
        ];
        if ( $survey ) {
            $array['question_1'] = isset( $survey->pasaporte ) ? trim( $survey->pasaporte ) : null;
            $array['question_2'] = isset( $survey->tramite_solicitud ) ? ( trim( $survey->tramite_solicitud ) == 'No' )  ? "No" : "Si" : null;
            $array['question_3'] = isset( $survey->tramite_agilidad ) ? trim( $survey->tramite_agilidad ) : null;
            $array['question_4'] = isset( $survey->horario ) ? trim( $survey->horario ) : null;
        }
        return $array;
    }

    public function getSuperCadeId( $name )
    {
        $data = SuperCade::query()->where('vc_nombre', 'LIKE', "%{$name}%")->first();
        return isset( $data->i_pk_id ) ? $data->i_pk_id : null;
    }

    public function getEps( $id )
    {
        $data = Eps::query()->where('Id_Eps', $id)->first();
        return isset( $data->Nombre_Eps ) ? $data->Nombre_Eps : null;
    }

    public function getDocumentType( $id )
    {
        $data = Documento::query()->where('Id_TipoDocumento', $id)->first();
        return isset( $data->Nombre_TipoDocumento ) ? $data->Nombre_TipoDocumento : null;
    }

    public function getGender( $id )
    {
        $data = Genero::query()->where('Id_Genero', $id)->first();
        return isset( $data->Nombre_Genero ) ? $data->Nombre_Genero : null;
    }

    public function getCountry( $id )
    {
        $data = Pais::query()->where('Id_Pais', $id)->first();
        return isset( $data->Nombre_Pais ) ? $data->Nombre_Pais : null;
    }

    public function getCity( $id )
    {
        $data = Ciudad::query()->where('Id_Ciudad', $id)->first();
        return isset( $data->Nombre_Ciudad ) ? $data->Nombre_Ciudad : null;
    }

    public function getLocation( $id )
    {
        $data = Localidad::query()->where('Id_Localidad', $id)->first();
        return isset( $data->Nombre_Localidad ) ? $data->Nombre_Localidad : null;
    }

    public function getActivities( $id )
    {
        $data = ActividadInteres::query()->where('i_pk_id', $id)->first();
        return isset( $data->vc_nombre ) ? $data->vc_nombre : null;
    }

    public function toUpper( $string = null )
    {
        return mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_UPPER, 'UTF-8');
    }
}