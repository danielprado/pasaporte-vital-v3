<?php

namespace App\Http\Controllers\Passport;

use App\ActividadInteres;
use App\Ciudad;
use App\DatosPersonaAnterior;
use App\Pasaporte;
use App\PasaporteAnterior;
use App\PasaporteEncuestaAnterior;
use App\PasaporteView;
use App\Persona;
use App\Renew;
use App\SuperCade;
use App\Transformers\ActivityTransformer;
use App\Transformers\CityTransformer;
use App\Transformers\CountryTransformer;
use App\Transformers\DocumentTransformer;
use App\Transformers\EpsTransformer;
use App\Transformers\LocationTransformer;
use App\Transformers\PassportTransformer;
use App\Transformers\PassportViewTransformer;
use App\Transformers\SuperCadeTransformer;
use Carbon\Carbon;
use Idrd\Usuarios\Repo\Documento;
use Idrd\Usuarios\Repo\Eps;
use Idrd\Usuarios\Repo\Localidad;
use Idrd\Usuarios\Repo\Pais;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class PassportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $column = \request()->has( 'column' ) ? \request()->get('column') : 'id';
        $order = \request()->has( 'order' ) ? \request()->get('order') : false;
        $per_page = \request()->has( 'per_page' ) ? \request()->get('per_page') : 5;
        $query = \request()->has( 'query' ) ? \request()->get('query') : null;

        $order =  ( $order ) ? 'asc' : 'desc';

        $passports = PasaporteView::query();

        if ( $query ) {
            $passports = $passports->where('id', 'LIKE', "%{$query}%")
                ->orWhere('user_cade', 'LIKE', "%{$query}%")
                ->orWhere('user', 'LIKE', "%{$query}%")
                ->orWhere('supercade', 'LIKE', "%{$query}%")
                ->orWhere('retired', 'LIKE', "%{$query}%")
                ->orWhere('location', 'LIKE', "%{$query}%")
                ->orWhere('stratum', 'LIKE', "%{$query}%")
                ->orWhere('address', 'LIKE', "%{$query}%")
                ->orWhere('phone', 'LIKE', "%{$query}%")
                ->orWhere('mobile', 'LIKE', "%{$query}%")
                ->orWhere('hobbies', 'LIKE', "%{$query}%")
                ->orWhere('eps', 'LIKE', "%{$query}%")
                ->orWhere('observations', 'LIKE', "%{$query}%")
                ->orWhere('question_1', 'LIKE', "%{$query}%")
                ->orWhere('question_2', 'LIKE', "%{$query}%")
                ->orWhere('question_3', 'LIKE', "%{$query}%")
                ->orWhere('question_4', 'LIKE', "%{$query}%")
                ->orWhere('created_at', 'LIKE', "%{$query}%")
                ->orWhere('hobbies_name', 'LIKE', "%{$query}%")
                ->orWhere('supercade_name', 'LIKE', "%{$query}%")
                ->orWhere('eps_name', 'LIKE', "%{$query}%")
                ->orWhere('document', 'LIKE', "%{$query}%")
                ->orWhere('document_type', 'LIKE', "%{$query}%")
                ->orWhere('full_name', 'LIKE', "%{$query}%")
                ->orWhere('birthday', 'LIKE', "%{$query}%")
                ->orWhere('country', 'LIKE', "%{$query}%")
                ->orWhere('gender', 'LIKE', "%{$query}%")
                ->orWhere('city', 'LIKE', "%{$query}%")
                ->orWhere('document_type_name', 'LIKE', "%{$query}%")
                ->orWhere('gender_name', 'LIKE', "%{$query}%")
                ->orWhere('country_name', 'LIKE', "%{$query}%")
                ->orWhere('city_name', 'LIKE', "%{$query}%")
                ->orWhere('location_name', 'LIKE', "%{$query}%")
                ->orWhere('user_cade_name', 'LIKE', "%{$query}%")
                ->orWhere('user_cade_document', 'LIKE', "%{$query}%");
        }
        $passports = $passports->orderBy( $column, $order )->paginate( $per_page );

        $resource = $passports->getCollection()
            ->map(function($model) {
                return ( new PassportViewTransformer )->transform( $model );
            })->toArray();

        $data = new LengthAwarePaginator(
            $resource,
            (int) $passports->total(),
            (int) $passports->perPage(),
            (int) $passports->currentPage(), [
            'path' => request()->url(),
            'query' => [
                'page' => (int) $passports->currentPage()
            ]
        ]);

        return response()->json([
            'data'  =>  $data,
            'code'  =>  200
        ], 200);
    }

    public function count()
    {
        $new = (int) PasaporteView::count();
        $old = (int) PasaporteAnterior::count();

        $renew = (int) Renew::whereDate('created_at', '>=', '2019-05-09 00:00:00')->count();

        $new_today = (int) PasaporteView::whereBetween('created_at', [ Carbon::now()->startOfDay(), Carbon::now()->endOfDay() ])->count();
        $old_today = (int) PasaporteAnterior::whereBetween('fechaExpedicion', [ Carbon::now()->startOfDay(), Carbon::now()->endOfDay() ])->count();

        $new_month = (int) PasaporteView::whereBetween('created_at', [ Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth() ])->count();
        $old_month = (int) PasaporteAnterior::whereBetween('fechaExpedicion', [ Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth() ])->count();

        $supercades = PasaporteView::query()
                                    ->select(DB::raw('count(*) as count, supercade_name'))->groupBy('supercade')
                                    ->whereBetween('created_at', [ Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth() ])->get();

        $mine = PasaporteView::query()->where('user_cade', $_SESSION['Id_funcionario'] )->whereBetween('created_at', [ Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth() ])->count();

        $total = $new + $old;
        $total_renew = $renew;
        $total_today = $new_today + $old_today;
        $total_month = $new_month + $old_month;

        return response()->json([
            'data'  => [
                'total' => (int) $total,
                'renew' => (int) $total_renew,
                'today' => (int) $total_today,
                'month' => (int) $total_month,
                'mine'  =>  (int) $mine,
                'supercades' => $supercades
            ],
            'code'  =>  200,
        ], 200);
    }

    public function initialSelects()
    {
        return response()->json([
            'data'  => [
                'document' => $this->document(),
                'supercade' => $this->suoercade(),
                'country' => $this->country(),
                'location' => $this->location(),
                'eps' => $this->eps(),
                'activities' => $this->activities(),
                'city' => $this->city( 41 ),
            ],
            'code'  =>  200,
        ], 200);
    }

    public function city($country)
    {
        $data = Ciudad::where('Id_Pais', $country)->get();
        if ( count( $data ) == 0 ) {
            $data = Ciudad::where('Id_Ciudad', 1123)->get();
        }
        $resource = new Collection($data, new CityTransformer);
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return $rootScope->toArray()['data'];
    }

    public function activities()
    {
        $data = ActividadInteres::all();
        $resource = new Collection($data, new ActivityTransformer);
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return $rootScope->toArray()['data'];
    }

    public function eps()
    {
        $data = \App\Eps::all();
        $resource = new Collection($data, new EpsTransformer);
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return $rootScope->toArray()['data'];
    }

    public function location()
    {
        $data = Localidad::all();
        $resource = new Collection($data, new LocationTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);

        return $rootScope->toArray()['data'];
    }

    public function country()
    {
        $data = Pais::all();
        $resource = new Collection($data, new CountryTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);

        return $rootScope->toArray()['data'];
    }

    public function suoercade()
    {
        if ( isset( $_SESSION['Usuario'][0] )  ) {
            $user_cades = DB::table( 'ususario_supercade' )->where( 'user_id', $_SESSION['Usuario'][0] )->pluck( 'supercade_id' );
        }

        if ( isset( $user_cades ) ) {
            $supercades = SuperCade::whereIn('i_pk_id', $user_cades)->get();
        } else {
            $supercades = SuperCade::all();
        }
        $resource = new Collection($supercades, new SuperCadeTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);

        return $rootScope->toArray()['data'];
    }

    public function document()
    {
        $documents = Documento::whereIn('Id_TipoDocumento',[1,4,6,15])->get();
        $resource = new Collection($documents, new DocumentTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);

        return $rootScope->toArray()['data'];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\StorePassportRequest $request
     * @return Response
     */
    public function store(Requests\StorePassportRequest $request)
    {
        $birthday = Carbon::parse( $request->get('birthday') );
        $year = Carbon::now()->diffInYears( $birthday );
        $retired  = ( $request->has('retired') ) ? $request->get('retired') : 'No';
        $gender   = ( $request->has('gender') ) ? $request->get('gender') : null;
        $document = ( $request->has('document') ) ? $request->get('document') : null;

        if( $retired =='No' && $year < 55  &&  $gender == 2 )
        {
            return response()->json([
                'message' =>  trans('validation.passport.female', ['age' => $year ]),
                'code'  =>  422
            ], 422);
        }

        if($retired =='No' && $year < 60  &&  $gender == 1 )
        {
            return response()->json([
                'message' =>  trans('validation.passport.male', ['age' => $year ]),
                'code'  =>  422
            ], 422);
        }

        $person = Persona::with('tipo')->where('Cedula', $request->get('document'))->first();

        if ( $person ) {
            $person->Cedula             = $request->get('document');
            $person->Primer_Nombre      = $request->get('first_name');
            $person->Segundo_Nombre     = $request->get('middle_name');
            $person->Primer_Apellido    = $request->get('first_last_name');
            $person->Segundo_Apellido   = $request->get('second_last_name');
            $person->Fecha_Nacimiento   = $request->get('birthday');
            $person->Id_Pais            = $request->get('country');
            $person->i_fk_id_ciudad     = $request->get('city');
            $person->Id_TipoDocumento   = $request->get('document_type');
            $person->Id_Genero          = $request->get('gender');
            $person->Id_Etnia           = 4;
            $person->save();
        } else {
            $person = new Persona();
            $person->Cedula             = $request->get('document');
            $person->Primer_Nombre      = $request->get('first_name');
            $person->Segundo_Nombre     = $request->get('middle_name');
            $person->Primer_Apellido    = $request->get('first_last_name');
            $person->Segundo_Apellido   = $request->get('second_last_name');
            $person->Fecha_Nacimiento   = $request->get('birthday');
            $person->Id_Pais            = $request->get('country');
            $person->i_fk_id_ciudad     = $request->get('city');
            $person->Id_TipoDocumento   = $request->get('document_type');
            $person->Id_Genero          = $request->get('gender');
            $person->Id_Etnia           = 4;
            $person->save();
            $person->tipo()->attach(6);
        }

        $passport   = Pasaporte::where('i_fk_id_usuario', $person->Id_Persona )->first();
        $old_passport = PasaporteAnterior::where('documento', $request->get('document') )->first();
        $datos_anteriores = DatosPersonaAnterior::find( $request->get('document') );

        if ( $passport ) {
            $passport->i_fk_id_usuario               = $person->Id_Persona;
            $passport->i_fk_id_usuario_supercade     = $_SESSION['Id_funcionario'];
            $passport->i_fk_id_superCade             = $request->get('supercade');
            $passport->vc_pensionado                 = $request->get('retired');
            $passport->i_fk_id_localidad             = $request->get('location');
            $passport->i_fk_id_estrato               = $request->get('stratum');
            $passport->vc_direccion                  = $request->get('address');
            $passport->vc_telefono                   = $request->get('phone');
            $passport->vc_celular                    = $request->get('mobile');
            $passport->i_fk_id_actividades           = $request->get('hobbies');
            $passport->i_fk_id_eps                   = $request->get('eps');
            $passport->email                         = Str::lower($request->get('email'));
            $passport->tx_observacion                = $request->get('observations');
            $passport->i_pregunta1                   = $request->get('question_1');
            $passport->i_pregunta2                   = $request->get('question_2');
            $passport->i_pregunta3                   = $request->get('question_3');
            $passport->i_pregunta4                   = $request->get('question_4');
            $passport->save();
        } else {
            if ( $old_passport ) {
                $old_passport->documento        =  $request->get('document');
                $old_passport->fechaExpedicion  =  Carbon::now()->format('Y-m-d');
                $old_passport->horaExpedicion   =  Carbon::now()->format('H:i:s');
                $old_passport->estadoPasaporte  =  'ACTIVO';
                $old_passport->impreso          =  'SI';
                $old_passport->id_Operario      =  $this->getPersona( $_SESSION['Id_funcionario'] )['document'];
                $old_passport->operario         =  $this->getPersona( $_SESSION['Id_funcionario'] )['name'];
                $old_passport->supercade        =  $this->getSuperCadeName( $request->get('supercade') );
                $old_passport->save();

                $survey = PasaporteEncuestaAnterior::query()->where('documento', $request->get('document'))->first();
                $survey = ($survey)  ? $survey : new PasaporteEncuestaAnterior();

                if ( $survey ) {
                    $survey->pasaporte          = $request->get('question_1');
                    $survey->tramite_solicitud  = $request->get('question_2');
                    $survey->tramite_agilidad   = $request->get('question_3');
                    $survey->horario            = $request->get('question_4');
                    $survey->fecha              = Carbon::now()->format('Y-m-d H:i:s');
                    $survey->save();
                }


            } else {
                $passport =  new Pasaporte();
                $passport->i_fk_id_usuario               = $person->Id_Persona;
                $passport->i_fk_id_usuario_supercade     = $_SESSION['Id_funcionario'];
                $passport->i_fk_id_superCade             = $request->get('supercade');
                $passport->vc_pensionado                 = $request->get('retired');
                $passport->i_fk_id_localidad             = $request->get('location');
                $passport->i_fk_id_estrato               = $request->get('stratum');
                $passport->vc_direccion                  = $request->get('address');
                $passport->vc_telefono                   = $request->get('phone');
                $passport->vc_celular                    = $request->get('mobile');
                $passport->i_fk_id_actividades           = $request->get('hobbies');
                $passport->i_fk_id_eps                   = $request->get('eps');
                $passport->tx_observacion                = $request->get('observations');
                $passport->i_pregunta1                   = $request->get('question_1');
                $passport->i_pregunta2                   = $request->get('question_2');
                $passport->i_pregunta3                   = $request->get('question_3');
                $passport->i_pregunta4                   = $request->get('question_4');
                $passport->save();
            }
        }

        if ( $datos_anteriores ) {
            $datos_anteriores->documento = $request->get('document');
            $datos_anteriores->apellidos = $request->get('first_last_name').' '.$request->get('second_last_name');
            $datos_anteriores->nombres = $request->get('first_name').' '.$request->get('middle_name');
            $datos_anteriores->tipoDocumento = $request->get('document_type');
            $datos_anteriores->fechaNacimiento = $request->get('birthday');
            $datos_anteriores->paisNacimiento = $request->get('country');
            $datos_anteriores->localidad = $request->get('location');
            $datos_anteriores->estrato = $request->get('stratum');
            $datos_anteriores->direccionResidencia = $request->get('address');
            $datos_anteriores->telFijo = $request->get('phone');
            $datos_anteriores->telCelular = $request->get('mobile');
            $datos_anteriores->email = $request->get('email');
            $datos_anteriores->pensionado = $request->get('retired');
            $datos_anteriores->actividadesInteres = $request->get('hobbies');
            $datos_anteriores->tipoSeguridad = $request->get('eps');
            $datos_anteriores->sexo = $request->get('gender');
            $datos_anteriores->observaciones = $request->get('observations');
            $datos_anteriores->save();
        }

        $passport_id = isset( $passport->i_pk_id )
                        ? $passport->i_pk_id
                        : null;

        return response()->json([
            'data'  =>  trans('validation.handler.success', ['passport' => $passport_id]),
            'passport'  =>  $passport_id,
            'code'  => 200
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param PasaporteView $passport
     * @return Response
     */
    public function show(PasaporteView $passport)
    {
        $resource = new Item($passport, new PassportViewTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $passport
     * @return Response
     */
    public function destroy($passport)
    {
        Renew::query()->where('i_fk_id_pasaporte', $passport->id )->delete();
        Pasaporte::find( $passport->id )->delete();
        return response()->json([
            'data'  => trans('validation.handler.deleted', ['passport' => $passport->id]),
            'code'  =>  200
        ],200);
    }

    public function getPersona( $document )
    {
        $person = Persona::query()->where('Id_Persona', $document)->first();
        $array = [
            'id'    =>  null,
            'name'  =>  null,
            'document'  =>  null,
        ];
        if ( $person ) {
            $array['id'] = isset( $person->Id_Persona ) ? (int) $person->Id_Persona : 0;
            $array['name'] = isset( $person->Primer_Nombre ) ? $this->toUpper( $person->Primer_Nombre ) : null;
            $array['document'] = isset( $person->Cedula ) ? $this->toUpper( $person->Cedula ) : 0;
        }

        return $array;
    }

    public function getSuperCadeName( $name )
    {
        $data = SuperCade::query()->where('i_pk_id', $name)->first();
        return isset( $data->vc_nombre ) ? $data->vc_nombre : null;
    }

    public function toUpper( $string = null )
    {
        return mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_UPPER, 'UTF-8');
    }
}
