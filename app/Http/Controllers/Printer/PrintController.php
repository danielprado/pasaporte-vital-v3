<?php

namespace App\Http\Controllers\Printer;


use App\Pasaporte;
use App\Renew;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PrintController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Requests\PrintRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(Requests\PrintRequest $request)
    {
        if ( $request->has('supercade_id') ) {

            if ( isset( $_SESSION['Id_funcionario'] ) ) {
                $passport = Pasaporte::query()->where('i_pk_id', $request->get('passport'))->first();
                if ( isset( $passport->printed ) ) {
                    if ( $passport->printed == true ) {
                        $renew = new Renew();
                        $renew->i_fk_id_usuario             =  $request->get('user_id');
                        $renew->i_fk_id_pasaporte           =  $request->get('passport');
                        $renew->i_fk_id_usuario_supercade   =  $_SESSION['Id_funcionario'];
                        $renew->i_fk_supercade              =  $request->get('supercade_id');
                        if ( $request->has('denounce') ) {
                            $renew->vc_denuncio                 =  $request->get('denounce');
                        }
                        if ( $renew->save() ) {
                            $pdf = \PDF::loadView('passport',[ 'request' => $request->all() ]);
                            $pdf->setPaper([0, 0, 155.9, 246.6], 'landscape');//15.59, 24.66
                            return $pdf->stream("Passport_{$request->get('passport')}");
                        }
                    } else {
                        $passport->printed = true;
                        if ( $passport->save() ) {
                            $pdf = \PDF::loadView('passport',[ 'request' => $request->all() ]);
                            $pdf->setPaper([0, 0, 155.9, 246.6], 'landscape');//15.59, 24.66
                            return $pdf->stream("Passport_{$request->get('passport')}");
                        }
                        return view('errors.500');
                    }
                } else {
                    $renew = new Renew();
                    $renew->i_fk_id_usuario             =  $request->get('user_id');
                    $renew->i_fk_id_pasaporte           =  $request->get('passport');
                    $renew->i_fk_id_usuario_supercade   =  $_SESSION['Id_funcionario'];
                    $renew->i_fk_supercade              =  $request->get('supercade_id');
                    if ( $request->has('denounce') ) {
                        $renew->vc_denuncio                 =  $request->get('denounce');
                    }
                    if ( $renew->save() ) {
                        $pdf = \PDF::loadView('passport',[ 'request' => $request->all() ]);
                        $pdf->setPaper([0, 0, 155.9, 246.6], 'landscape');//15.59, 24.66
                        return $pdf->stream("Passport_{$request->get('passport')}");
                    }
                }
            }

            return view('errors.401');
        }

        return view( 'errors.422' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
