<?php

namespace App\Http\Controllers\Report;

use App\Http\Requests\GenerateReportRequest;
use App\Http\Requests\OwnReporteRequest;
use App\Http\Requests\RenewReporteRequest;
use App\Http\Requests\Request;
use App\PasaporteView;
use App\Renew;
use App\Transformers\PassportViewTransformer;
use App\Transformers\RenewTransformer;
use Carbon\Carbon;

use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_Border;

class ReportController extends Controller
{
    public function general(GenerateReportRequest $request)
    {
        $start = $request->has('start_date') ? Carbon::parse( $request->get('start_date') )->startOfDay()->format('Y-m-d H:i:s') : Carbon::now()->startOfMonth()->startOfDay()->format('Y-m-d H:i:s');
        $end   = $request->has('end_date') ? Carbon::parse( $request->get('end_date') )->endOfDay()->format('Y-m-d H:i:s') : Carbon::now()->endOfMonth()->endOfDay()->format('Y-m-d H:i:s');
        if ( $end >= $start ) {
            $passports = PasaporteView::query()->whereBetween('created_at', [ $start, $end ]);
            $resource = $passports->orderBy('created_at')->get()
                        ->map(function($model) {
                            return ( new PassportViewTransformer )->transform( $model );
                        })->toArray();

             return $this->fillExcel( $resource, $start, $end );
        }

        return view('errors.422');
    }

    public function own(OwnReporteRequest $request)
    {
        $start = $request->has('start_date') ? Carbon::parse( $request->get('start_date') )->startOfDay()->format('Y-m-d H:i:s') : Carbon::now()->startOfMonth()->startOfDay()->format('Y-m-d H:i:s');
        $end   = $request->has('end_date') ? Carbon::parse( $request->get('end_date') )->endOfDay()->format('Y-m-d H:i:s') : Carbon::now()->endOfMonth()->endOfDay()->format('Y-m-d H:i:s');
        if ( $end >= $start ) {
            $passports = PasaporteView::query()->where('user_cade',  $_SESSION['auth']->Id_Persona )->whereBetween('created_at', [ $start, $end ]);
            $resource = $passports->orderBy('created_at')->get()
                ->map(function($model) {
                    return ( new PassportViewTransformer )->transform( $model );
                })->toArray();

            return $this->fillExcel( $resource, $start, $end );
        }

        return view('errors.422');
    }

    public function renew(RenewReporteRequest $request)
    {
        $start = $request->has('start_date') ? Carbon::parse( $request->get('start_date') )->startOfDay()->format('Y-m-d H:i:s') : Carbon::now()->startOfMonth()->startOfDay()->format('Y-m-d H:i:s');
        $end   = $request->has('end_date') ? Carbon::parse( $request->get('end_date') )->endOfDay()->format('Y-m-d H:i:s') : Carbon::now()->endOfMonth()->endOfDay()->format('Y-m-d H:i:s');
        if ( $end >= $start ) {
            $renews = Renew::query()->whereBetween('created_at', [ $start, $end ]);
            $resource = $renews->orderBy('created_at')->get()
                ->map(function($model) {
                    return ( new RenewTransformer() )->transform( $model );
                })->toArray();

            return $this->fillRenews( $resource, $start, $end );
        }
        return view('errors.422');
    }

    public function fillRenews( $data, $star, $end )
    {
        Excel::load( public_path('excel/REPORTE_RENOVACIONES.xlsx'), function ($file) use ( $data, $star, $end ) {
            $file->sheet(0, function ($sheet) use ( $data, $star, $end ) {
                $row = 8;

                $sheet->cell("B3", function($cell) use ($row)  {
                    $cell->setValue( isset( $_SESSION['auth']->full_name ) ? $_SESSION['auth']->full_name : '' );
                });
                $sheet->cell("B4", function($cell) use ($row, $star, $end )  {
                    $cell->setValue( "Del  $star al $end " );
                });
                $sheet->cell("B5", function($cell) use ($row, $star, $end )  {
                    $cell->setValue( Carbon::now()->format('Y-m-d H:i:s') );
                });

                foreach ( $data as $datum ) {
                    $date = isset( $datum['created_at'] ) ?  Carbon::parse( $datum['created_at'] ) : null;
                    $arr = [
                        isset( $datum['passport'] ) ? $this->toUpper( $datum['passport'] ) : '',
                        isset( $datum['user_cade_name'] ) ? $this->toUpper( $datum['user_cade_name'] ) : '',
                        isset( $datum['supercade_name'] ) ? $this->toUpper( $datum['supercade_name'] ) : '',
                        isset( $datum['document'] ) ? $this->toUpper( $datum['document'] ) : '',
                        isset( $datum['user_name'] ) ? $this->toUpper( $datum['user_name'] ) : '',
                        isset( $datum['denounce'] ) ? (string) $this->toUpper( $datum['denounce'] ) : '',
                        isset( $date )     ? $date->format('Y-m-d') : '',
                        isset( $date )     ? $date->format('H:i:s') : '',
                    ];
                    $sheet->row($row, $arr);
                    $row++;
                }

                $sum = $row-1;

                $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                );

                $sheet->getStyle("A8:H$sum")->applyFromArray($styleArray);

            });
        })->export('xlsx');
    }

    public function fillExcel( $data, $star, $end )
    {
        Excel::load( public_path('excel/REPORTE_PASAPORTE_VITAL.xlsx'), function ($file) use ( $data, $star, $end ) {
            $file->sheet(0, function ($sheet) use ( $data, $star, $end ) {
                $row = 8;

                $sheet->cell("B3", function($cell) use ($row)  {
                    $cell->setValue( isset(  $_SESSION['auth']->full_name ) ?  $_SESSION['auth']->full_name : '' );
                });
                $sheet->cell("B4", function($cell) use ($row, $star, $end )  {
                    $cell->setValue( "Del  $star al $end " );
                });
                $sheet->cell("B5", function($cell) use ($row, $star, $end )  {
                    $cell->setValue( Carbon::now()->format('Y-m-d H:i:s') );
                });

                foreach ( $data as $datum ) {

                    $date = isset( $datum['created_at'] ) ?  Carbon::parse( $datum['created_at'] ) : null;

                    $arr = [
                        isset( $datum['user_cade_name'] ) ? $this->toUpper( $datum['user_cade_name'] ) : '',
                        isset( $datum['supercade_name'] ) ? $this->toUpper( $datum['supercade_name'] ) : '',
                        isset( $date )     ? $date->format('Y-m-d') : '',
                        isset( $date )     ? $date->format('H:i:s') : '',
                        isset( $datum['id'] )             ? $this->toUpper( $datum['id'] ) : '',
                        isset( $datum['first_name'] )     ? $this->toUpper( $datum['first_name'] ) : '',
                        isset( $datum['middle_name'] )    ? $this->toUpper( $datum['middle_name'] ) : '',
                        isset( $datum['first_last_name'] ) ? $this->toUpper( $datum['first_last_name'] ) : '',
                        isset( $datum['second_last_name'] ) ? $this->toUpper( $datum['second_last_name'] ) : '',
                        isset( $datum['document_type_name'] ) ? $this->toUpper( $datum['document_type_name'] ) : '',
                        isset( $datum['document'] ) ? $this->toUpper( $datum['document'] ) : '',
                        isset( $datum['age'] ) ? $this->toUpper( $datum['age'] ) : '',
                        isset( $datum['retired'] ) ? $this->toUpper( $datum['retired'] ) : '',
                        isset( $datum['location_name'] ) ? $this->toUpper( $datum['location_name'] ) : '',
                        isset( $datum['stratum'] ) ? $this->toUpper( $datum['stratum'] ) : '',
                        isset( $datum['address'] ) ? $this->toUpper( $datum['address'] ) : '',
                        isset( $datum['phone'] ) ? $this->toUpper( $datum['phone'] ) : '',
                        isset( $datum['mobile'] ) ? $this->toUpper( $datum['mobile'] ) : '',
                        isset( $datum['hobbies_name'] ) ? $this->toUpper( $datum['hobbies_name'] ) : '',
                        isset( $datum['eps_name'] ) ? $this->toUpper( $datum['eps_name'] ) : '',
                        isset( $datum['observations'] ) ? $this->toUpper( $datum['observations'] ) : '',
                        isset( $datum['question_1'] ) ? $this->toUpper( $datum['question_1'] ) : '',
                        isset( $datum['question_2'] ) ? $this->toUpper( $datum['question_2'] ) : '',
                        isset( $datum['question_3'] ) ? $this->toUpper( $datum['question_3'] ) : '',
                        isset( $datum['question_4'] ) ? $this->toUpper( $datum['question_4'] ) : '',
                        isset( $datum['renew'] ) ? ( (int) $datum['renew'] > 1 ) ? (int) $datum['renew'] - 1 : 0 : 0
                    ];
                    $sheet->row($row, $arr);
                    $row++;
                }

                $sum = $row-1;

                $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                );

                $sheet->getStyle("A8:Z$sum")->applyFromArray($styleArray);

            });
        })->export('xlsx');
    }

    public function toUpper( $string = null )
    {
        return mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_UPPER, 'UTF-8');
    }
}
