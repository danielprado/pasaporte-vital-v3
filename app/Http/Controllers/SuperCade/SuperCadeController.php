<?php

namespace App\Http\Controllers\SuperCade;

use App\SuperCade;
use App\Transformers\SuperCadeTransformer;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class SuperCadeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ( isset( $_SESSION['Usuario'][0] )  ) {
            $user_cades = DB::table( 'ususario_supercade' )->where( 'user_id', $_SESSION['Usuario'][0] )->pluck( 'supercade_id' );
        }

        if ( isset( $user_cades ) ) {
            $supercades = SuperCade::whereIn('i_pk_id', $user_cades)->get();
        } else {
            $supercades = SuperCade::all();
        }
        $resource = new Collection($supercades, new SuperCadeTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);

        return response()->json($rootScope->toArray(), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
