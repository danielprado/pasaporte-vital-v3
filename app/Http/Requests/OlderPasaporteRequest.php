<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OlderPasaporteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function forbiddenResponse()
    {
        return \request()->wantsJson()
            ? response()->json([
                'message' =>  trans('validation.handler.unauthorized'),
                'code'  =>  403
            ], 403)
            : view('errors.403');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
