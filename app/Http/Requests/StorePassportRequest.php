<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StorePassportRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function forbiddenResponse()
    {
        return \request()->wantsJson()
                ? response()->json([
                        'message' =>  trans('validation.handler.unauthorized'),
                        'code'  =>  403
                    ], 403)
                : view('errors.403');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document_type' =>  'required',
            'document'  =>  'required',
            'supercade' =>  'required',
            'retired'   =>  'required',
            'gender'    =>  'required',
            'first_name'    =>  'required',
            'first_last_name'   =>  'required',
            'second_last_name'  =>  'string',
            'birthday' =>  'required',
            'country'   =>  'required',
            'city'  =>  'required',
            'location'  =>  'required',
            'stratum'   =>  'required',
            'address'   =>  'required',
            'phone' =>  'digits_between:7,10',
            'mobile'    =>  'digits:10',
            'hobbies'   =>  'required',
            'email'   =>  'email',
            'eps'   =>  'required',
            'question_1'   =>  'required',
            'question_2'   =>  'required',
            'question_3'   =>  'required',
            'question_4'   =>  'required',
        ];
    }
}
