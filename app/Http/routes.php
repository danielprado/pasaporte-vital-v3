<?php
session_start();
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//rutas con filtro de autenticación
//Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'MainController@welcome')->name('welcome');
    Route::any('/welcome', 'MainController@index');
    Route::post('login', 'MainController@login');
    Route::any('/logout', 'MainController@logout')->name('logout');
	Route::group(['middleware' => ['check.auth'] ], function () {
        Route::post('api/user/check', 'MainController@check');
        Route::post('/export/general', 'Report\ReportController@general');
        Route::post('/export/own', 'Report\ReportController@own');
        Route::post('/export/renew', 'Report\ReportController@renew');
        Route::post('/passport/print', 'Printer\PrintController@index');
        /* Routes for Vue */
        Route::group(['prefix' =>   'api'], function () {
            Route::get('user', 'MainController@auth');
            Route::get('passport/count', 'Passport\PassportController@count');
            Route::get('initial-data', 'Passport\PassportController@initialSelects');
            Route::resource('passport', 'Passport\PassportController', [
                'except'  =>  ['create', 'edit']
            ]);
            Route::get('older-passport', 'Passport\OlderPassportController@index');
            Route::delete('older-passport/{id}', 'Passport\OlderPassportController@destroy');
            Route::resource('super-cade', 'SuperCade\SuperCadeController', [
                'only'  =>  ['index']
            ]);
            Route::resource('document', 'Document\DocumentController', [
                'only'  =>  ['index']
            ]);
            Route::get('companies/table', 'Company\CompanyController@table');
            Route::resource('companies', 'Company\CompanyController', [
                'only'  =>  ['index', 'store']
            ]);
            Route::get('agreements/table', 'Agreement\AgreementController@table');
            Route::get('agreements/{company}', 'Agreement\AgreementController@index');
            Route::post('agreements/create', 'Agreement\AgreementController@create');
            Route::delete('agreements/{agreement}/user', 'Agreement\AgreementController@destroyAgreementUser');
            Route::resource('agreements', 'Agreement\AgreementController', [
                'only'  =>  ['store', 'destroy', 'update']
            ]);
            Route::resource('eps', 'Eps\EpsController', [
                'only'  =>  ['index']
            ]);
            Route::resource('activities', 'Activities\ActivitiesController', [
                'only'  =>  ['index']
            ]);
            Route::get('country/{name}', 'Country\CountryController@find');
            Route::resource('country', 'Country\CountryController', [
                'only'  =>  ['index']
            ]);
            Route::resource('country.city', 'City\CityController', [
                'only'  =>  ['index']
            ]);
            Route::resource('location', 'Location\LocationController', [
                'only'  =>  ['index']
            ]);
        });
    });
//});
