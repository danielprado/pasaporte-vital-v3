<?php

namespace App;

use Idrd\Usuarios\Repo\Eps;
use Idrd\Usuarios\Repo\Localidad;
use Illuminate\Database\Eloquent\Model;

class Pasaporte extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_usuarios_pasaporte';

    /**
     * The primary key for the model.
     *
     * @var string
     */
	protected $primaryKey = 'i_pk_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
	    'i_fk_id_usuario_supercade',
        'i_fk_id_usuario',
        'i_fk_id_superCade',
        'vc_pensionado',
        'i_fk_id_localidad',
        'i_fk_id_estrato',
        'vc_direccion',
        'vc_telefono',
        'vc_celular',
        'i_fk_id_actividades',
        'i_fk_id_eps',
        'email',
        'tx_observacion',
        'i_pregunta1',
        'i_pregunta2',
        'i_pregunta3',
        'i_pregunta4'
    ];

	 public function user_passport() {
        return $this->belongsTo(Persona::class,'i_fk_id_usuario');
    }

    public function user_cade() {
        return $this->belongsTo(Persona::class,'i_fk_id_usuario_supercade');
    }

    public function supercade() {
        return $this->belongsTo(SuperCade::class,'i_fk_id_superCade');
    }

     public function location() {
        return $this->belongsTo(Localidad::class,'i_fk_id_localidad');
    }

     public function activity() {
        return $this->belongsTo(ActividadInteres::class,'i_fk_id_actividades');
    }

    public function eps() {
        return $this->belongsTo(Eps::class,'i_fk_id_eps');
    }
}

