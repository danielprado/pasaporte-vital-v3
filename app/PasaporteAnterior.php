<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class PasaporteAnterior extends Eloquent {

    protected $table = 'pasaporte_pasaporte';
    protected $primaryKey = 'idPasaporte';
    protected $fillable = ['documento'];
    protected $connection = 'pasaporte_anterior';
    public $timestamps = false;

}