<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class PasaporteEncuestaAnterior extends Eloquent {

    protected $table = 'pasaporte_encuesta';
    protected $primaryKey = 'documento';
    protected $fillable = ['pasaporte', 'tramite_solicitud', 'tramite_agilidad', 'horario'];
    protected $connection = 'pasaporte_anterior';
    public $timestamps = false;

}