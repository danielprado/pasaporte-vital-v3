<?php

namespace App;

use Idrd\Usuarios\Repo\Eps;
use Idrd\Usuarios\Repo\Localidad;
use Illuminate\Database\Eloquent\Model;

class PasaporteView extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'nuevos_pasaportes_view';

    /**
     * The primary key for the model.
     *
     * @var string
     */
	protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
        'user_cade',
        'user',
        'supercade',
        'retired',
        'location',
        'stratum',
        'address',
        'phone',
        'mobile',
        'hobbies',
        'eps',
        'observations',
        'question_1',
        'question_2',
        'question_3',
        'question_4',
        'hobbies_name',
        'supercade_name',
        'eps_name',
        'document',
        'document_type',
        'full_name',
        'birthday',
        'country',
        'gender',
        'city',
        'document_type_name',
        'gender_name',
        'country_name',
        'city_name',
        'user_cade_name',
        'user_cade_document',
        'created_at'
    ];
}

