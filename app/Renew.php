<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Renew extends Model
{
    protected $table = 'tbl_renovacion';
    protected $primaryKey = 'i_pk_id';
    protected $fillable = ['i_fk_id_pasaporte','i_fk_id_usuario','i_fk_id_usuario','i_fk_id_usuario_supercade','i_fk_supercade'];
    public $timestamps = true;
}
