<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuperCade extends Model
{
    protected $table = 'tbl_supercades';
    protected $primaryKey = 'i_pk_id';
    protected $fillable = ['vc_nombre','i_estado'];
    protected $connection = '';
    public $timestamps = true;
}
