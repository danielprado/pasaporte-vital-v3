<?php


namespace App\Transformers;


use App\ActividadInteres;
use League\Fractal\TransformerAbstract;

class ActivityTransformer extends TransformerAbstract
{
    public function transform(ActividadInteres $actividadInteres)
    {
        return [
            'id'      =>  isset( $actividadInteres->i_pk_id ) ?   $actividadInteres->i_pk_id : 0,
            'name'    =>  isset( $actividadInteres->vc_nombre ) ? $actividadInteres->vc_nombre : null,
        ];
    }
}