<?php


namespace App\Transformers;


use App\Agreement;
use League\Fractal\TransformerAbstract;

class AgreementTransformer extends TransformerAbstract
{
    public function transform( Agreement $agreement )
    {
        return [
            'id'         => isset( $agreement->id )         ? (int) $agreement->id : null,
            'name'       => isset( $agreement->agreement )  ? $agreement->agreement : null,
            'company_id' => isset( $agreement->company_id ) ? (int) $agreement->company_id : null,
            'created_at' => isset( $agreement->created_at ) ? $agreement->created_at->format('Y-m-d H:i:s') : null,
            'updated_at' => isset( $agreement->updated_at ) ? $agreement->updated_at->format('Y-m-d H:i:s') : null
        ];
    }
}