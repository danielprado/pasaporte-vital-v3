<?php


namespace App\Transformers;


use App\AgreementsView;
use App\Persona;
use League\Fractal\TransformerAbstract;

class AgreementViewTransformer extends TransformerAbstract
{
    public function transform( AgreementsView $view )
    {
        return [
            'id'        =>  isset( $view->id ) ? $view->id : null,
            'company'   =>  isset( $view->company ) ? $view->company : null,
            'agreement' =>  isset( $view->agreement ) ? $view->agreement : null,
            'user_id'   =>  isset( $view->user_id ) ? $view->user_id : null,
            'user'      => isset( $view->user ) ? $view->user  : null,
            'beneficiary_id'   =>  isset( $view->beneficiary_id ) ? $view->beneficiary_id : null,
            'beneficiary'      => isset( $view->beneficiary ) ? $view->beneficiary : null,
            'created_at'    =>  isset( $view->created_at ) ? $view->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $view->updated_at ) ? $view->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }

    public function getPersona( $id = null )
    {
        if ( isset( $id ) ) {
            $person = Persona::query()->where('Id_Persona', $id)->first();
            return isset( $person->full_name ) ? $person->full_name : null;
        }

        return null;
    }
}