<?php


namespace App\Transformers;


use Idrd\Usuarios\Repo\Ciudad;
use League\Fractal\TransformerAbstract;

class CityTransformer extends TransformerAbstract
{
    public function transform(Ciudad $ciudad)
    {
        return [
            'id'      =>  isset( $ciudad->Id_Ciudad ) ?     $ciudad->Id_Ciudad : 0,
            'name'    =>  isset( $ciudad->Nombre_Ciudad ) ? $ciudad->Nombre_Ciudad : null,
            'country' =>  isset( $ciudad->Id_Pais ) ? $ciudad->Id_Pais : null,
        ];
    }
}