<?php


namespace App\Transformers;


use App\CompanyAgreementView;
use League\Fractal\TransformerAbstract;

class CompanyAgreementViewTransformer extends TransformerAbstract
{
    public function transform( CompanyAgreementView $view )
    {
        return [
            'id'        =>  isset( $view->id ) ? $view->id : null,
            'company'   =>  isset( $view->company ) ? $view->company : null,
            'company_id'=>  isset( $view->company_id ) ? (int) $view->company_id : null,
            'agreement' =>  isset( $view->agreement ) ? $view->agreement : null,
            'created_at'    =>  isset( $view->created_at ) ? $view->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset( $view->updated_at ) ? $view->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }
}