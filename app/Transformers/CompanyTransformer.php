<?php


namespace App\Transformers;


use App\Company;
use League\Fractal\TransformerAbstract;

class CompanyTransformer extends TransformerAbstract
{
    public function transform( Company $company )
    {
        return [
            'id'         => isset( $company->id ) ? (int) $company->id : null,
            'name'       => isset( $company->company ) ?  $company->company : null,
            'created_at' => isset( $company->created_at ) ? $company->created_at->format('Y-m-d H:i:s') : null,
            'updated_at' => isset( $company->updated_at ) ? $company->updated_at->format('Y-m-d H:i:s') : null
        ];
    }
}