<?php


namespace App\Transformers;


use Idrd\Usuarios\Repo\Pais;
use League\Fractal\TransformerAbstract;

class CountryTransformer extends TransformerAbstract
{
    public function transform(Pais $pais)
    {
        return [
            'id'      =>  isset( $pais->Id_Pais ) ? $pais->Id_Pais : 0,
            'name'    =>  isset( $pais->Nombre_Pais ) ? $pais->Nombre_Pais : null,
        ];
    }
}