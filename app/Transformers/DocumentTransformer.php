<?php


namespace App\Transformers;



use Idrd\Usuarios\Repo\Documento;
use League\Fractal\TransformerAbstract;

class DocumentTransformer extends TransformerAbstract
{
    public function transform( $documento )
    {
        return [
            "id"            =>  isset( $documento->Id_TipoDocumento ) ? $documento->Id_TipoDocumento : 0,
            "name"          =>  isset( $documento->Nombre_TipoDocumento ) ? $documento->Nombre_TipoDocumento : null,
            "description"   =>  isset( $documento->Descripcion_TipoDocumento ) ? $documento->Descripcion_TipoDocumento : null,
        ];
    }
}