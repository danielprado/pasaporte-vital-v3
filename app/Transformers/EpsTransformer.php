<?php


namespace App\Transformers;



use App\Eps;
use League\Fractal\TransformerAbstract;

class EpsTransformer extends TransformerAbstract
{
    public function transform(Eps $eps)
    {
        return [
            'id'      =>  isset( $eps->i_pk_id ) ?   $eps->i_pk_id : 0,
            'name'    =>  isset( $eps->vc_nombre ) ? $eps->vc_nombre : null,
        ];
    }
}