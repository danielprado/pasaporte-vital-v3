<?php


namespace App\Transformers;


use App\Pasaporte;
use League\Fractal\TransformerAbstract;

class PassportTransformer extends TransformerAbstract
{
    public function transform( Pasaporte $pasaporte )
    {
        return [
            "id"            =>  isset( $pasaporte->i_pk_id ) ? (int) $pasaporte->i_pk_id : 0,

            "document_type" =>  isset( $pasaporte->user_passport->Id_TipoDocumento ) ? $pasaporte->user_passport->Id_TipoDocumento : null,
            "document_type_name" =>  isset( $pasaporte->user_passport->tipoDocumento->Nombre_TipoDocumento ) ? $pasaporte->user_passport->tipoDocumento->Nombre_TipoDocumento : null,
            "document"      =>  isset( $pasaporte->user_passport->Cedula ) ? $pasaporte->user_passport->Cedula : null,


            "supercade"     =>  isset( $pasaporte->i_fk_id_superCade ) ? $pasaporte->i_fk_id_superCade : null,
            "supercade_name"     =>  isset( $pasaporte->supercade->vc_nombre ) ? $pasaporte->supercade->vc_nombre : null,

            "retired"       =>  isset( $pasaporte->vc_pensionado ) ? $pasaporte->vc_pensionado : null,

            "gender"        =>  isset( $pasaporte->user_passport->Id_Genero ) ? $pasaporte->user_passport->Id_Genero : null,
            "gender_name"        =>  isset( $pasaporte->user_passport->genero->Nombre_Genero ) ? $pasaporte->user_passport->genero->Nombre_Genero : null,

            "first_last_name"   =>  isset( $pasaporte->user_passport->Primer_Apellido ) ? $pasaporte->user_passport->Primer_Apellido : null,
            "second_last_name"  =>  isset( $pasaporte->user_passport->Segundo_Apellido ) ? $pasaporte->user_passport->Segundo_Apellido : null,
            "first_name"    =>  isset( $pasaporte->user_passport->Primer_Nombre ) ? $pasaporte->user_passport->Primer_Nombre : null,
            "middle_name"   =>  isset( $pasaporte->user_passport->Segundo_Nombre ) ? $pasaporte->user_passport->Segundo_Nombre : null,
            "full_name"   =>  isset( $pasaporte->user_passport->full_name ) ? $pasaporte->user_passport->full_name : null,


            "birthdate" =>  isset( $pasaporte->user_passport->Fecha_Nacimiento ) ? $pasaporte->user_passport->Fecha_Nacimiento : null,

            "country"   =>  isset( $pasaporte->user_passport->Id_Pais ) ? $pasaporte->user_passport->Id_Pais : null,
            "country_name"   =>  isset( $pasaporte->user_passport->pais->Nombre_Pais ) ? $pasaporte->user_passport->pais->Nombre_Pais : null,
            "city"      =>  isset( $pasaporte->user_passport->i_fk_id_ciudad ) ? $pasaporte->user_passport->i_fk_id_ciudad : null,
            "city_name"      =>  isset( $pasaporte->user_passport->ciudad->Nombre_Ciudad ) ? $pasaporte->user_passport->ciudad->Nombre_Ciudad : null,

            "location"  =>  isset( $pasaporte->i_fk_id_localidad ) ? $pasaporte->i_fk_id_localidad : null,
            "location_name"  =>  isset( $pasaporte->location->Nombre_Localidad ) ? $pasaporte->location->Nombre_Localidad : null,

            "stratus"   =>  isset( $pasaporte->i_fk_id_estrato ) ? (int) $pasaporte->i_fk_id_estrato : null,
            "address"   =>  isset( $pasaporte->vc_direccion ) ? $pasaporte->vc_direccion : null,
            "phone"     =>  isset( $pasaporte->vc_telefono ) ? $pasaporte->vc_telefono : null,
            "mobile"    =>  isset( $pasaporte->vc_celular ) ? $pasaporte->vc_celular : null,
            "email"    =>  isset( $pasaporte->email ) ? $pasaporte->email : null,

            "hobbies"       =>  isset( $pasaporte->i_fk_id_actividades ) ? $pasaporte->i_fk_id_actividades : null,
            "hobbies_name"  =>  isset( $pasaporte->activity->vc_nombre ) ? $pasaporte->activity->vc_nombre : null,
            "eps"       =>  isset( $pasaporte->i_fk_id_eps ) ? $pasaporte->i_fk_id_eps : null,
            "eps_name"       =>  isset( $pasaporte->eps->Nombre_Eps ) ? $pasaporte->eps->Nombre_Eps : null,

            "observation"   =>  isset( $pasaporte->tx_observacion ) ? $pasaporte->tx_observacion : null,
            "how_know"  =>  isset( $pasaporte->i_pregunta1 ) ? $pasaporte->i_pregunta1 : null,
            "benefits"  =>  isset( $pasaporte->i_pregunta2 ) ? $pasaporte->i_pregunta2 : null,
            "attention" =>  isset( $pasaporte->i_pregunta3 ) ? $pasaporte->i_pregunta3 : null,
            "agility"   =>  isset( $pasaporte->i_pregunta4 ) ? $pasaporte->i_pregunta4 : null,
        ];
    }
}