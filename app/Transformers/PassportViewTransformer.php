<?php


namespace App\Transformers;


use App\PasaporteView;
use App\Renew;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class PassportViewTransformer extends TransformerAbstract
{
    public function transform( $view )
    {
        return [
            'id'    =>  isset( $view->id ) ? (int) $view->id : null,
            'user_cade' =>  isset( $view->user_cade ) ? (int) $view->user_cade : null,
            'user'  =>  isset( $view->user ) ? (int) $view->user : null,
            'supercade' =>  isset( $view->supercade ) ? (int) $view->supercade : null,
            'retired'   =>  isset( $view->retired ) ? $view->retired : null,
            'location'  =>  isset( $view->location ) ? (int) $view->location : null,
            'stratum'   =>  isset( $view->stratum ) ? (int) $view->stratum : null,
            'address'   =>  isset( $view->address ) ? $this->toUpper( $view->address ) : null,
            'phone' =>  isset( $view->phone ) ? $view->phone : null,
            'mobile'    =>  isset( $view->mobile ) ? $view->mobile : null,
            'hobbies'   =>  isset( $view->hobbies ) ? (int) $view->hobbies : null,
            'eps'   =>  isset( $view->eps ) ? (int) $view->eps : null,
            'observations'  =>  isset( $view->observations ) ? $view->observations : null,
            'question_1'    =>  isset( $view->question_1 ) ? trim( $view->question_1 ) : null,
            'question_2'    =>  isset( $view->question_2 ) ? ($view->question_2 == 'Si') ? $view->question_2 : 'No' : null,
            'question_3'    =>  isset( $view->question_3 ) ? trim( $view->question_3 ) : null,
            'question_4'    =>  isset( $view->question_4 ) ? trim( $view->question_4 ) : null,
            'hobbies_name'    =>  isset( $view->hobbies_name ) ? $view->hobbies_name : null,
            'supercade_name'  =>  isset( $view->supercade_name ) ? $view->supercade_name : null,
            'eps_name'  =>  isset( $view->eps_name ) ? $view->eps_name : null,
            'document' =>  isset( $view->document ) ? (int) $view->document : null,
            'document_type' =>  isset( $view->document_type ) ? (int) $view->document_type : null,
            'full_name'  =>  isset( $view->full_name ) ? $this->toUpper( $view->full_name ) : null,
            'print_name'    => isset( $view->first_name,  $view->first_last_name) ? "{$this->toUpper( $view->first_name )} {$this->toUpper( $view->first_last_name )} {$this->toUpper( $view->second_last_name )}" : null,
            'first_name'  =>  isset( $view->first_name ) ? $this->toUpper( $view->first_name ) : null,
            'middle_name'  =>  isset( $view->middle_name ) ? $this->toUpper( $view->middle_name ) : null,
            'first_last_name'  =>  isset( $view->first_last_name ) ? $this->toUpper( $view->first_last_name ) : null,
            'second_last_name'  =>  isset( $view->second_last_name ) ? $this->toUpper( $view->second_last_name ) : null,
            'birthday'   =>  isset( $view->birthday ) ? $view->birthday : null,
            'email'   =>  isset( $view->email ) ? $view->email : null,
            'age'   =>  isset( $view->birthday ) ?  (int) Carbon::now()->diffInYears( Carbon::parse( $view->birthday ) ) : null,
            'country'    =>  isset( $view->country ) ? (int) $view->country : null,
            'gender'  =>  isset( $view->gender ) ? (int) $view->gender : null,
            'city'    =>  isset( $view->city ) ? (int) $view->city : null,
            'document_type_name'   =>  isset( $view->document_type_name ) ? $view->document_type_name : null,
            'gender_name'  =>  isset( $view->gender_name ) ? $view->gender_name : null,
            'country_name' =>  isset( $view->country_name ) ? $view->country_name : null,
            'city_name'    =>  isset( $view->city_name ) ? $view->city_name : null,
            'location_name'    =>  isset( $view->location_name ) ? $view->location_name : null,
            'user_cade_name' =>  isset( $view->user_cade_name ) ? $this->toUpper( $view->user_cade_name ) : null,
            'user_cade_document'    =>  isset( $view->user_cade_document) ? $view->user_cade_document : null,
            'renew'                 => Renew::query()->where('i_fk_id_pasaporte', isset( $view->id ) ? (int) $view->id : 0)->get()->count(),
            'created_at'    =>  isset( $view->created_at ) ? $view->created_at->format( 'Y-m-d H:i:s' ) : null,
        ];
    }

    public function toUpper( $string = null )
    {
        return mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_UPPER, 'UTF-8');
    }

}