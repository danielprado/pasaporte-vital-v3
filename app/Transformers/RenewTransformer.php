<?php


namespace App\Transformers;


use App\Persona;
use App\Renew;
use App\SuperCade;
use League\Fractal\TransformerAbstract;

class RenewTransformer extends TransformerAbstract
{
    public function transform( Renew $renew )
    {
        $person = isset( $renew->i_fk_id_usuario ) ? $this->getPersona( $renew->i_fk_id_usuario ) : null;
        return [
            'id'        =>  isset( $renew->i_pk_id ) ? $renew->i_pk_id : null,
            'passport'  =>  isset( $renew->i_fk_id_pasaporte ) ? $renew->i_fk_id_pasaporte : null,
            'user' =>  isset( $renew->i_fk_id_usuario ) ? $renew->i_fk_id_usuario : null,
            'user_cade' =>  isset( $renew->i_fk_id_usuario_supercade ) ? $renew->i_fk_id_usuario_supercade : null,
            'user_cade_name'    =>  isset( $renew->i_fk_id_usuario_supercade ) ? $this->getPersona( $renew->i_fk_id_usuario_supercade )['name'] : null,
            'supercade'          =>  isset( $renew->i_fk_supercade ) ? $renew->i_fk_supercade : null,
            'supercade_name'    =>  isset( $renew->i_fk_supercade ) ? $this->getSuperCadeName( $renew->i_fk_supercade ) : null,
            'document'         =>  isset( $person['document'] ) ? $person['document'] : null,
            'user_name'         =>  isset( $person['name'] ) ? $person['name'] : null,
            'denounce'          => isset( $renew->vc_denuncio ) ? $renew->vc_denuncio : null,
            'created_at'        =>  isset( $renew->created_at ) ? $renew->created_at->format('Y-m-d H:i:s') : null
        ];
    }

    public function getPersona( $document )
    {
        $person = Persona::query()->where('Id_Persona', $document)->first();
        $array = [
            'id'    =>  null,
            'name'  =>  null,
            'document'  =>  null,
        ];
        if ( $person ) {
            $array['id'] = isset( $person->Id_Persona ) ? (int) $person->Id_Persona : 0;
            $array['name'] = isset( $person->full_name ) ? $this->toUpper( $person->full_name ) : null;
            $array['document'] = isset( $person->Cedula ) ? $this->toUpper( $person->Cedula ) : 0;
        }

        return $array;
    }

    public function getSuperCadeName( $name )
    {
        $data = SuperCade::query()->where('i_pk_id', $name)->first();
        return isset( $data->vc_nombre ) ? $data->vc_nombre : null;
    }

    public function toUpper( $string = null )
    {
        return mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_UPPER, 'UTF-8');
    }
}