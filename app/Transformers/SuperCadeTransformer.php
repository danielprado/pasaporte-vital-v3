<?php


namespace App\Transformers;


use App\SuperCade;
use League\Fractal\TransformerAbstract;

class SuperCadeTransformer extends TransformerAbstract
{
    public function transform( SuperCade $superCade )
    {
        return [
            'id'        =>  isset( $superCade->i_pk_id ) ? (int) $superCade->i_pk_id : 0,
            'name'      =>  isset( $superCade->vc_nombre ) ? $superCade->vc_nombre : 0,
            'status'    =>  isset( $superCade->i_estado ) ? (boolean) $superCade->i_estado : 0,
        ];
    }
}