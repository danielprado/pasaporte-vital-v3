(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard/Auth/Lock.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Dashboard/Auth/Lock.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components */ "./resources/js/components/index.js");
/* harmony import */ var _models_services_User__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/models/services/User */ "./resources/js/models/services/User.js");
/* harmony import */ var _models_Api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/models/Api */ "./resources/js/models/Api.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Lock",
  components: {
    LockCard: _components__WEBPACK_IMPORTED_MODULE_0__["LockCard"]
  },
  data: function data() {
    return {
      name: null,
      error: null,
      token: document.head.querySelector('meta[name="csrf-token"]'),
      api: _models_Api__WEBPACK_IMPORTED_MODULE_2__["Api"].END_POINTS.LOGIN(),
      user: new _models_services_User__WEBPACK_IMPORTED_MODULE_1__["User"]({
        username: null,
        password: null
      }),
      image: "".concat("http://localhost:8000", "/public/img/faces/user.png")
    };
  },
  created: function created() {
    this.name = this.$store.getters.getUsername || this.$tc('Passport', 1);
    this.user.username = this.$auth.auth();

    if (this.user.username === null) {
      this.$router.push({
        name: 'Login'
      });
    }
  },
  methods: {
    onLogin: function onLogin() {
      var _this = this;

      this.$validator.validateAll().then(function (isValid) {
        if (isValid) {
          _this.user.login().then(function () {
            _this.$store.dispatch('login').then(function () {
              window.isAuthenticated = true;

              _this.$router.push({
                name: 'Dashboard'
              });
            });
          }).catch(function (error) {
            _this.error = error.message;

            _this.$notify({
              message: error.message || _this.unexpected,
              icon: "add_alert",
              horizontalAlign: 'top',
              verticalAlign: 'center',
              type: 'danger'
            });
          });
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard/Auth/Lock.vue?vue&type=template&id=20f9b1fe&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Dashboard/Auth/Lock.vue?vue&type=template&id=20f9b1fe& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "md-layout text-center" }, [
    _c("div", { staticClass: "md-layout-item md-size-50 md-small-size-100" }, [
      _c(
        "form",
        { attrs: { action: _vm.api, method: "post" } },
        [
          _c(
            "lock-card",
            [
              _c("img", {
                staticClass: "img",
                attrs: { slot: "imageProfile", src: _vm.image },
                slot: "imageProfile"
              }),
              _vm._v(" "),
              _c(
                "h4",
                {
                  staticClass: "title",
                  attrs: { slot: "title" },
                  slot: "title"
                },
                [_vm._v(_vm._s(_vm.name))]
              ),
              _vm._v(" "),
              _c(
                "md-field",
                {
                  class: [
                    {
                      "md-error":
                        _vm.errors.has(_vm.$t("password")) || _vm.error
                    }
                  ],
                  attrs: { slot: "content" },
                  slot: "content"
                },
                [
                  _c("label", { attrs: { for: "password" } }, [
                    _vm._v(_vm._s(_vm.$t("Enter Password")))
                  ]),
                  _vm._v(" "),
                  _c("md-input", {
                    directives: [
                      {
                        name: "validate",
                        rawName: "v-validate",
                        value: "required",
                        expression: "'required'"
                      }
                    ],
                    attrs: {
                      "data-vv-name": _vm.$t("password"),
                      name: "password",
                      id: "password",
                      type: "password"
                    },
                    model: {
                      value: _vm.user.password,
                      callback: function($$v) {
                        _vm.$set(_vm.user, "password", $$v)
                      },
                      expression: "user.password"
                    }
                  }),
                  _vm._v(" "),
                  _c("input", {
                    attrs: { type: "hidden", name: "_token" },
                    domProps: { value: _vm.token.content }
                  }),
                  _vm._v(" "),
                  _c("input", {
                    attrs: { type: "hidden", name: "username" },
                    domProps: { value: _vm.user.username }
                  }),
                  _vm._v(" "),
                  _c("input", {
                    attrs: { type: "hidden", name: "password" },
                    domProps: { value: _vm.user.password }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "md-button",
                {
                  staticClass: "md-rounded md-info md-lg",
                  attrs: {
                    slot: "footer",
                    "native-type": "submit",
                    type: "submit"
                  },
                  slot: "footer"
                },
                [
                  _vm._v(
                    "\n                    " +
                      _vm._s(_vm.$t("Unlock")) +
                      "\n                "
                  )
                ]
              )
            ],
            1
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/pages/Dashboard/Auth/Lock.vue":
/*!****************************************************!*\
  !*** ./resources/js/pages/Dashboard/Auth/Lock.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Lock_vue_vue_type_template_id_20f9b1fe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Lock.vue?vue&type=template&id=20f9b1fe& */ "./resources/js/pages/Dashboard/Auth/Lock.vue?vue&type=template&id=20f9b1fe&");
/* harmony import */ var _Lock_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Lock.vue?vue&type=script&lang=js& */ "./resources/js/pages/Dashboard/Auth/Lock.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Lock_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Lock_vue_vue_type_template_id_20f9b1fe___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Lock_vue_vue_type_template_id_20f9b1fe___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/Dashboard/Auth/Lock.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/Dashboard/Auth/Lock.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ./resources/js/pages/Dashboard/Auth/Lock.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Lock_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Lock.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard/Auth/Lock.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Lock_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/Dashboard/Auth/Lock.vue?vue&type=template&id=20f9b1fe&":
/*!***********************************************************************************!*\
  !*** ./resources/js/pages/Dashboard/Auth/Lock.vue?vue&type=template&id=20f9b1fe& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Lock_vue_vue_type_template_id_20f9b1fe___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Lock.vue?vue&type=template&id=20f9b1fe& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard/Auth/Lock.vue?vue&type=template&id=20f9b1fe&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Lock_vue_vue_type_template_id_20f9b1fe___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Lock_vue_vue_type_template_id_20f9b1fe___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);