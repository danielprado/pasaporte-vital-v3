import "./bootstrap";
import Vue from "vue";
import store from "./store";
import Auth from "@/package/auth"
import "./registerServiceWorker";
import DashboardPlugin from "./material-dashboard";

// Plugins
import App from "./App.vue";
import Chartist from "chartist";
import VueCookies from 'vue-cookies'
Vue.use(VueCookies);

import i18n from "./i18n";
import { VclCode, VclTable, VclFacebook, VclInstagram, VclList} from "vue-content-loading";
import VueContentLoading from "vue-content-loading";

import Vuetify from 'vuetify'
import theme from "./theme"
import 'vuetify/dist/vuetify.min.css'

// plugin setup
Vue.use(DashboardPlugin);
Vue.use(Vuetify, {
  theme
});
Vue.use(Auth);

Vue.component( 'vcl-code', VclCode );
Vue.component( 'vcl-table', VclTable );
Vue.component( 'vcl-facebook', VclFacebook );
Vue.component( 'vcl-instagram', VclInstagram );
Vue.component( 'vcl-list', VclList );
Vue.component( 'vue-content-loading', VueContentLoading );

import router from "./router";


String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
};


router.beforeEach((to, from, next) => {
  if ( typeof window.source == 'function') {
    window.source('Canceled by the user');
  }
  if (to.matched.some(record => record.meta.forVisitors)) {
    if (Vue.auth.isAuthenticated()) {
      next({name: 'Home'})
    } else if ( to.matched.some((record) => record.meta.can) ) {
      next();
    } else {
      next({name: 'Home'});
    }
  } else if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!Vue.auth.isAuthenticated()) {
      next({name: 'Lock'});
    } else if ( to.matched.some((record) => record.meta.can) ) {
      next();
    } else {
      next({name: 'Home'})
    }
  } else {
    next();
  }
});

// global library setup
Vue.prototype.$Chartist = Chartist;

Vue.config.productionTip = false;

Vue.mixin({
  data: () => {
    return {
      refCount: 0,
      isLoading: false,
      unexpected: null,
    }
  },
  mounted: function () {
    if ( window.isAuthenticated === false && this.$auth.auth() ) {
      this.$router.push({name: 'Lock'});
    }
  
    if ( window.isAuthenticated === false && !this.$auth.auth() ) {
      this.$router.push({name: 'Login'});
    }
    
    this.unexpected = this.$t('unexpected failure');
    this.$material.locale = {
      startYear: 1900,
      endYear: 2099,
      dateFormat: 'YYYY-MM-DD',
      days: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
      shortDays: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
      shorterDays: ['D', 'L', 'M', 'X', 'J', 'V', 'S'],
      months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Augosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      shortMonths: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dic'],
      shorterMonths: ['E', 'F', 'M', 'A', 'M', 'J', 'Ju', 'A', 'S', 'O', 'N', 'D'],
      firstDayOfAWeek: 0
    };
  },
  methods: {
    setLoading(isLoading) {
      if (isLoading) {
        this.refCount++;
        this.isLoading = true;
      } else if (this.refCount > 0) {
        this.refCount--;
        this.isLoading = (this.refCount > 0);
      }
    },
    axiosInterceptor: function () {
      window.axios.interceptors.request.use((config) => {
        this.setLoading( true );
        return config;
      }, (error) => {
        this.setLoading(false);
        return Promise.reject(error);
      });

      window.axios.interceptors.response.use((response) => {
        this.setLoading(false);
        return response;
      }, (error) => {
        this.setLoading(false);
        return Promise.reject(error);
      });
    }
  }
});

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount("#app");
