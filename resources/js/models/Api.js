export const Api = {
    COOKIE: process.env.MIX_COOKIE_NAME,
    END_POINTS: {
        INITIAL_DATA: () => { return `${process.env.MIX_IDRD_URL}/api/initial-data` },
        CADES: () => { return `${process.env.MIX_IDRD_URL}/api/super-cade` },
        COMPANIES: () => { return `${process.env.MIX_IDRD_URL}/api/companies` },
        AGREEMENTS: ( id = null ) => { return id ? `${process.env.MIX_IDRD_URL}/api/agreements/${id}` : `${process.env.MIX_IDRD_URL}/api/agreements` },
        DOCUMENT: () => { return `${process.env.MIX_IDRD_URL}/api/document` },
        COUNTRY: () => { return `${process.env.MIX_IDRD_URL}/api/country` },
        EPS: () => { return `${process.env.MIX_IDRD_URL}/api/eps` },
        ACTIVITY: () => { return `${process.env.MIX_IDRD_URL}/api/activities` },
        /**
         * @return {string}
         */
        CITY: function (id) {
            return `${process.env.MIX_IDRD_URL}/api/country/${id}/city`;
        },
        LOCATION: () => { return `${process.env.MIX_IDRD_URL}/api/location` },
        GENERAL_REPORT: () => { return `${process.env.MIX_IDRD_URL}/export/general` },
        OWN_REPORT: () => { return `${process.env.MIX_IDRD_URL}/export/own` },
        RENEW_REPORT: () => { return `${process.env.MIX_IDRD_URL}/export/renew` },
        PASSPORT: () => { return `${process.env.MIX_IDRD_URL}/api/passport` },
        OLDER_PASSPORT: () => { return `${process.env.MIX_IDRD_URL}/api/older-passport` },
        PRINT: () => { return `${process.env.MIX_IDRD_URL}/api/print-passport` },
        PRINT_PASSPORT: () => { return `${process.env.MIX_IDRD_URL}/passport/print` },
        GET_PROFILE: () => { return `${process.env.MIX_IDRD_URL}/api/user` },
        CHECK_AUTH: () => { return `${process.env.MIX_IDRD_URL}/api/user/check` },
        LOGIN: () => { return `${process.env.MIX_IDRD_URL}/login` },
        LOGOUT: () => { return `${process.env.MIX_IDRD_URL}/logout` },
    }
};
