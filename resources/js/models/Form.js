import { Errors } from "./Errors";
import router from "@/router";
import store from "@/store"

export class Form {
  constructor(data) {
    this.originalData = data;

    for (let field in data) {
      this[field] = data[field];
    }
    this.errors = new Errors();
  }

  reset() {
    for (let field in this.originalData) {
      this[field] = this.originalData[field];
    }
    this.errors.clear();
  }

  data() {
    let data = {};
    for (let property in this.originalData) {
      data[property] = this[property];
    }
    return data;
  }
  submit(method, url, options = {}) {
    return new Promise((resolve, reject) => {
      method = method.toLowerCase();
      options = {
        ...options,
        cancelToken: new window.CancelToken(function executor(c) {
          // An executor function receives a cancel function as a parameter
          window.source = c;
        })
      };

      if (window.navigator.onLine) {
        let request =
          method !== "get" && method !== "delete"
            ? window.axios[method](url, this.data(), options)
            : window.axios[method](url, options);

        request
          .then(response => {
            this.onSuccess( (method !== "put") );
            resolve(response.data);
          })
          .catch(error => {
            if (window.axios.isCancel(error)) {
              console.log('Request canceled', error.message);
            } else {
              if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                if (process.env.NODE_ENV !== "production") {
                  console.log("Response");
                  console.log(error.response);
                }

                if ( error.response.status !== 422 && error.response.status !== 429 && error.response.status !== 404 ) {
                  router.push({
                    name: "Error",
                    params: {
                      code: error.response.status,
                      text: error.response.data.message || 'Unexpected Failure'
                    }
                  });
                }

                if ( error.response.status === 403 ) {
                  router.push({
                    name: "Error",
                    params: {
                      code: error.response.status,
                      text: error.response.data.message
                    }
                  });
                }

                if ( error.response.status === 401 ) {
                  if ( typeof window.source == 'function') {
                    window.source('Canceled by the user');
                  }
                  window.isAuthenticated = false;
                  if ( store.getters.getAuth ) {
                    router.push({
                      name: 'Lock'
                    });
                  } else {
                    router.push({
                      name: 'Login'
                    });
                  }
                }

                this.onFail(error.response.data);
                reject(error.response.data);

              } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                if (process.env.NODE_ENV !== "production") {
                  console.log("Request");
                  console.log(error.request);
                }
                reject(error.request);
              } else {
                // Something happened in setting up the request that triggered an Error
                console.error("Error", error.message || error);
                reject(error.message || error);
              }

              if ( process.env.NODE_ENV !== "production" ) {
                console.log('Config');
                console.log(error.config);
              }
              reject(error);
            }
          });
      } else {
        reject( { message: 'You are working in offline mode.', code: 422 } );
        console.info("You are working in offline mode.");
      }
    });
  }

  post(url, options = {}) {
    return this.submit("POST", url, options);
  }

  get(url, options = {}) {
    return this.submit("GET", url, options);
  }

  put(url, options = {}) {
    return this.submit("PUT", url, options);
  }

  delete(url, options = {}) {
    return this.submit("DELETE", url, options);
  }

  patch(url, options = {}) {
    return this.submit("PATCH", url, options);
  }

  onSuccess(reset = false) {
    if (reset) {
      this.reset();
    }
  }

  onFail(errors) {
    this.errors.record(errors);
  }
}
