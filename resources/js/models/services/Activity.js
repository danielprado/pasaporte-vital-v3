import { Model } from "../Model";
import { Api } from "../Api";

export class Activity extends Model {
    constructor(id, data) {
        super(Api.END_POINTS.ACTIVITY(), data);
    }
}
