import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class AgreementCompany extends Model {
  constructor() {
    super(Api.END_POINTS.AGREEMENTS('create'), {
      company_id: null,
      agreement: null
    });
  
    this.validations = {
      text: {
        required: true,
        min: 3
      }
    }
  }
  
  update(id, options = {}) {
    return this.put(Api.END_POINTS.AGREEMENTS(id), options);
  }
  
  delete(id, options = {}) {
    return super.delete(Api.END_POINTS.AGREEMENTS(id), options);
  }
}