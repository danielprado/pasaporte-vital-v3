import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Agreements extends Model {
  constructor( id = null ) {
    super(Api.END_POINTS.AGREEMENTS( id ), {
      beneficiary_id: null,
      passport: null,
      document: null,
      company: null,
      agreement: null,
    })
  }
}