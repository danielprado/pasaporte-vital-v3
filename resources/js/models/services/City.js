import { Model } from "../Model";
import { Api } from "../Api";

export class City extends Model {
    constructor(id, data) {
        super(Api.END_POINTS.CITY(id), data);
    }
}
