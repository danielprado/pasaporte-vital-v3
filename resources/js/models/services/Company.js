import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Company extends Model {
  constructor() {
    super( Api.END_POINTS.COMPANIES(), {
      company: null
    });
    
    this.validations = {
      text: {
        required: true,
        min: 3
      }
    }
  }

  table( options = {} ) {
    return this.get( `${Api.END_POINTS.COMPANIES()}/table`, options);
  }

}