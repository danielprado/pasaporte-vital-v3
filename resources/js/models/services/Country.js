import { Model } from "../Model";
import { Api } from "../Api";

export class Country extends Model {
    constructor(data) {
        super(Api.END_POINTS.COUNTRY(), data);
    }

    find(name) {
        return this.get( `${Api.END_POINTS.COUNTRY()}/${name}`);
    }
}
