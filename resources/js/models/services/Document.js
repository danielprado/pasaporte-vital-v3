import { Model } from "../Model";
import { Api } from "../Api";

export class Document extends Model {
    constructor(data) {
        super(Api.END_POINTS.DOCUMENT(), data);
    }
}
