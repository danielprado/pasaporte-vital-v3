import { Model } from "../Model";
import { Api } from "../Api";

export class Eps extends Model {
    constructor(data) {
        super(Api.END_POINTS.EPS(), data);
    }
}
