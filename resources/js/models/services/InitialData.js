import { Model } from "../Model";
import { Api } from "../Api";

export class InitialData extends Model {
    constructor(id, data) {
        super(Api.END_POINTS.INITIAL_DATA(), data);
    }
}
