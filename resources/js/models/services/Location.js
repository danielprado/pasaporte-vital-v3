import { Model } from "../Model";
import { Api } from "../Api";

export class Location extends Model {
    constructor(data) {
        super(Api.END_POINTS.LOCATION(), data);
    }
}
