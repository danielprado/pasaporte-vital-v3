import { Model } from "../Model";
import { Api } from "../Api";

export class OlderPassport extends Model {
    constructor(data) {
        super(Api.END_POINTS.OLDER_PASSPORT(), data);
    }
}