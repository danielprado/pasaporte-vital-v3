import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Passport extends Model {
  constructor(data) {
    super( Api.END_POINTS.PASSPORT(), data );
  }

  count() {
    return this.get( `${Api.END_POINTS.PASSPORT()}/count`, {} );
  }

  renew() {
    return this.get( `${Api.END_POINTS.PASSPORT()}/count`, {} );
  }

  today() {
    return this.get( `${Api.END_POINTS.PASSPORT()}/count`, {} );
  }

  month() {
    return this.get( `${Api.END_POINTS.PASSPORT()}/count`, {} );
  }
}
