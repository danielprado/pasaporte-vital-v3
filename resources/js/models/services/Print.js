import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Print extends Model {
    
    constructor(data) {
        super( Api.END_POINTS.PRINT(), data );
        this.validation = {
            query: {
                required: false,
                numeric: true,
                min: 3,
                max: 10
            }
        };
    }
}