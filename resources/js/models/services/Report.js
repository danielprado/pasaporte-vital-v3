import { Model } from "../Model";
import { Api } from "../Api";

export class Report extends Model {
    constructor(data) {
        super(Api.END_POINTS.GENERAL_REPORT(), data);
    }
}
