import { Model } from "../Model";
import { Api } from "../Api";

export class SuperCade extends Model {
    constructor(data) {
        super(Api.END_POINTS.CADES(), data);
    }
}
