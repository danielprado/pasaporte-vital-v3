import Vue from "vue";
import Auth from "@/package/auth"
Vue.use(Auth);
import DashboardLayout from "@/pages/Dashboard/Layout/DashboardLayout.vue";
const Passport = () =>
  import(/* webpackChunkName: "passport" */ "@/pages/Dashboard/Passport/Passport.vue");
const PassportTable = () =>
  import(/* webpackChunkName: "passport" */ "@/pages/Dashboard/Passport/PassportTable.vue");
const PassportEdit = () =>
  import(/* webpackChunkName: "passport" */ "@/pages/Dashboard/Passport/PassportEdit.vue");
const OlderPassport = () =>
    import(/* webpackChunkName: "passport" */ "@/pages/Dashboard/Passport/OlderPassport.vue");
const Print = () =>
    import(/* webpackChunkName: "passport" */ "@/pages/Dashboard/Passport/Print.vue");
const GeneralReport = () =>
    import(/* webpackChunkName: "passport" */ "@/pages/Dashboard/Passport/GeneralReportPassport.vue");
const OwnReport = () =>
    import(/* webpackChunkName: "passport" */ "@/pages/Dashboard/Passport/OwnReportPassport.vue");
const RenewReport = () =>
    import(/* webpackChunkName: "passport" */ "@/pages/Dashboard/Passport/RenewReportPassport.vue");
const Agreement = () =>
    import(/* webpackChunkName: "passport" */ "@/pages/Dashboard/Passport/Agreement.vue");
const CreateAgreement = () =>
    import(/* webpackChunkName: "passport" */ "@/pages/Dashboard/Passport/CreateAgreement.vue");
const AgreementReport = () =>
    import(/* webpackChunkName: "passport" */ "@/pages/Dashboard/Passport/AgreementReport.vue");

const passport = {
  path: "/passport",
  component: DashboardLayout,
  redirect: "/passport/create",
  name: "Passport",
  children: [
    {
      path: "create",
      name: "Create Passport",
      components: { default: Passport },
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('create_or_edit_passport'),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: "table",
      name: "Passport Table",
      components: { default: PassportTable },
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('view_passports'),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: "older",
      name: "Older Passport",
      components: { default: OlderPassport },
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('create_or_edit_passport'),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: ":id/edit",
      name: "Passport Edit",
      components: { default: PassportEdit },
      props: true,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('create_or_edit_passport'),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: "print",
      name: "Print",
      components: { default: Print },
      props: true,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('print_passport'),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: "report/general",
      name: "General Report",
      components: { default: GeneralReport },
      props: true,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('create_general_report'),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: "report/own",
      name: "Own Report",
      components: { default: OwnReport },
      props: true,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('create_own_report'),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: "report/renew",
      name: "Renew Report",
      components: { default: RenewReport },
      props: true,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('create_renew_report'),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: "agreements",
      name: "Agreements",
      components: { default: Agreement },
      props: true,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('view_agreement'),
      }
    },
    {
      path: "agreements/create",
      name: "Create Agreements",
      components: { default: CreateAgreement },
      props: true,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('create_agreement'),
      }
    },
    {
      path: "agreements/report",
      name: "Agreements Report",
      components: { default: AgreementReport },
      props: true,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('agreement_report'),
      }
    }
  ]
};

export default passport;
