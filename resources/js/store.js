import Vue from "vue";
import Vuex from "vuex";
//import Cookies from 'js-cookie';
import {Api} from "@/models/Api";
import AuthModule from "@/store/modules/auth";
import CounterModules from "@/store/modules/counters";
import SelectModules from "@/store/modules/selects";
import PreferencesModule from "@/store/modules/preferences"
//import createPersistedState from 'vuex-persistedstate'
import localForage from "localforage";
import VuexPersist from 'vuex-persist';
Vue.use(Vuex);

const vuexPersistEmitter = () => {
  return store => {
    store.subscribe(mutation => {
      if (mutation.type === 'RESTORE_MUTATION') {
        store._vm.$root.$emit('storageReady');
      }
    });
  }
};

const debug = process.env.NODE_ENV !== 'production';

localForage.config({
  name        : 'PasaporteVital',
  version     : 1.0,
  storeName   : Api.COOKIE,
  description : 'Almcena de forma offline algunos atributos de la base de datos.'
});

const vuexLocalStorage = new VuexPersist({
  strictMode: true,
  asyncStorage: false,
  key: Api.COOKIE,
  storage: localForage,
  modules: ['selects'],
});

const vuexLocalStorageSetting = new VuexPersist({
  strictMode: true,
  asyncStorage: false,
  key: Api.COOKIE,
  storage: window.localStorage,
  modules: ['settings'],
});

const vuexSessionStorage = new VuexPersist({
  strictMode: true,
  asyncStorage: false,
  key: Api.COOKIE,
  storage: window.sessionStorage,
  modules: ['user', 'counter'],
});

export default new Vuex.Store({
  mutations: {
    RESTORE_MUTATION: vuexLocalStorage.RESTORE_MUTATION // this mutation **MUST** be named "RESTORE_MUTATION"
  },
  modules: {
    user: AuthModule,
    counter: CounterModules,
    selects: SelectModules,
    settings: PreferencesModule
  },
  plugins: [
    vuexPersistEmitter(),
    vuexLocalStorage.plugin,
    vuexSessionStorage.plugin,
    vuexLocalStorageSetting.plugin,
    /*
    createPersistedState({
      key: Api.COOKIE,
      storage: {
        getItem: key => Cookies.getJSON(key),
        setItem: (key, value) => Cookies.set(key, value),
        removeItem: key => Cookies.remove(key)
      },
      getState: (key) => Cookies.getJSON(key),
      setState: (key, state) => Cookies.set(key, state),
      paths: ['settings'],
    })
     */
  ],
  strict: debug
})
