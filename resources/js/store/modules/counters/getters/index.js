const getters = {
    getTotal: state => ( state.counter_total ) ? state.counter_total : null,
    getRenew: state => ( state.counter_renew ) ? state.counter_renew : null,
    getToday: state => ( state.counter_today ) ? state.counter_today : null,
    getMonthly: state => ( state.counter_month ) ? state.counter_month : null,
    getMines: state => ( state.counter_mine ) ? state.counter_mine : null,
    getDataCades: state => ( state.data_cades ) ? state.data_cades : null,
};

export default getters;