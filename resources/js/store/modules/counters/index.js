import actions from '@/store/modules/counters/actions';
import getters from '@/store/modules/counters/getters';
import mutations from '@/store/modules/counters/mutations';

const state = {
    counter_total: 0,
    counter_renew: 0,
    counter_today: 0,
    counter_month: 0,
    counter_mine: 0,
    data_cades: [],
};

export default { actions, mutations, getters, state }

