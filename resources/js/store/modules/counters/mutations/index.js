const mutations = {
    COUNTERS( state, response ) {
        state.counter_total   = response.data.total;
        state.counter_renew   = response.data.renew;
        state.counter_today   = response.data.today;
        state.counter_month   = response.data.month;
        state.counter_mine    = response.data.mine;
        state.data_cades      = response.data.supercades;
    },
    RESET(state) {
        state.counter_total = null;
        state.counter_renew = null;
        state.counter_today = null;
        state.counter_month = null;
        state.counter_mine = null;
        state.data_cades   = null;
    },
};

export default mutations;