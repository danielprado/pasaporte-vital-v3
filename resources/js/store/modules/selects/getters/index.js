const getters = {
    getStoreActivities: state => ( state.activities ) ? state.activities : null,
    getStoreCities: state => ( state.city ) ? state.city : null,
    getStoreCountries: state => ( state.country ) ? state.country : null,
    getStoreDocuments: state => ( state.document ) ? state.document : null,
    getStoreEps: state => ( state.eps ) ? state.eps : null,
    getStoreLocations: state => ( state.location ) ? state.location : null,
    getStoreCades: state => ( state.supercade ) ? state.supercade : null
};

export default getters;