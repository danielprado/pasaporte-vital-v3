const mutations = {
    INITIAL_DATA( state, response ) {
        state.activities   = response.data.activities.map( (activity) => {
            return {
                id: activity.id,
                text: `${activity.name}`,
                all: activity
            }
        });
        state.city         = response.data.city.map( city => {
            return {
                id: city.id,
                text: `${city.name}`,
                all: city
            }
        });
        state.country      = response.data.country.map( country => {
            return {
                id: country.id,
                name: `${country.name}`,
            }
        });
        state.document     = response.data.document.map( document => {
            return {
                id: document.id,
                text: `${document.name} - ${document.description}`,
                all: document
            }
        });
        state.eps          = response.data.eps.map( (eps) => {
            return {
                id: eps.id,
                text: `${eps.name}`,
                all: eps
            }
        });
        state.location     = response.data.location.map( location => {
            return {
                id: location.id,
                text: `${location.name}`,
                all: location
            }
        });
        state.supercade    = response.data.supercade.map( document => {
            return {
                id: document.id,
                text: `${document.name}`,
                all: document
            }
        });
    },

    SET_ACTIVITIES(state, response) {
        state.activities = response.data.map( (activity) => {
            return {
                id: activity.id,
                text: `${activity.name}`,
                all: activity
            }
        });
    },
    SET_CITY(state, response) {
        state.city  = response.data.map( city => {
            return {
                id: city.id,
                text: `${city.name}`,
                all: city
            }
        });
    },
    SET_COUNTRY(state, response) {
        state.country = response.data.map( country => {
            return {
                id: country.id,
                name: `${country.name}`,
            }
        });
    },
    SET_DOCUMENT(state, response) {
        state.document = response.data.map( document => {
            return {
                id: document.id,
                text: `${document.name} - ${document.description}`,
                all: document
            }
        });
    },
    SET_EPS(state, response) {
        state.eps  = response.data.map( (eps) => {
            return {
                id: eps.id,
                text: `${eps.name}`,
                all: eps
            }
        });
    },
    SET_LOCATION(state, response) {
        state.location  = response.data.map( location => {
            return {
                id: location.id,
                text: `${location.name}`,
                all: location
            }
        });
    },
    SET_SUPERCADE(state, response) {
        state.supercade = response.data.map( document => {
            return {
                id: document.id,
                text: `${document.name}`,
                all: document
            }
        });
    },

    RESET_ACTIVITIES(state) {
        state.activities = null;
    },
    RESET_CITY(state) {
        state.city = null;
    },
    RESET_COUNTRY(state) {
        state.country = null;
    },
    RESET_DOCUMENT(state) {
        state.document = null;
    },
    RESET_EPS(state) {
        state.eps = null;
    },
    RESET_LOCATION(state) {
        state.location = null;
    },
    RESET_SUPERCADE(state) {
        state.supercade = null;
    },
    RESET_ALL_DATA(state) {
        state.activities   = null;
        state.city         = null;
        state.country      = null;
        state.document     = null;
        state.eps          = null;
        state.location     = null;
        state.supercade    = null;
    }
};

export default mutations;